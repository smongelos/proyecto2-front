app.controller('ModificarOperacionCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        $scope.selected.modificarDatos.monto =  formatear($scope.selected.modificarDatos.monto);
        tipo = 'operaciones';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#cambioOperacion').modal('toggle');
        }

        $scope.modificar = function () {

            var parametros = {
                "operacionNoEspecificada": $scope.selected.modificarDatos.operacionNoEspecificada,
                "monto": formatoMonto($scope.selected.modificarDatos.monto),
                "tipoOperacion": $scope.selected.modificarDatos.tipoOperacion,
                "idServicioRealizado": $scope.selected.modificarDatos.idServicioRealizado,
                "idCaja": $scope.selected.modificarDatos.idCaja,
                "idOrdenesCompra": $scope.selected.modificarDatos.idOrdeneenesCompra,
                "idOperaciones" : $scope.selected.modificarDatos.idOperaciones
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.info('Operación modificada con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.operaciones')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.info('Ha ocurrido un error, intente nuevamente.', 'Atención');
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.operaciones');
        };
    }]);