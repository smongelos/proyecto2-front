app.controller('OperacionesCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.personas;
        parametros = 'operaciones';
        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.operacion = response.data.lista;
                        console.log($scope.operacion)
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                idOperaciones: 'desc'
                            },
                            page: 1,
                            count: 7,
                        }, {
                                counts: [],
                                total: $scope.operacion.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.operacion, params.orderBy()) : $scope.operacion;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente.', 'Atención');
                    unBlockUI();
                });
        }

        $scope.listar();



        $scope.modificar = function (operacion) {
            $localStorage.modificarDatos = operacion;
            $state.go('app.modificar-operacion');
        };

        $scope.openModal = function (operacion) {
            $localStorage.eliminarDatos = operacion;
            $scope.selected = {};
            $scope.selected.eliminarDatos = $localStorage.eliminarDatos;
            $('#eliminarOperacion').modal('toggle');
        };

        $scope.eliminar = function (operacion) {
            var parametros = {
                "operacionNoEspecificada": $scope.selected.eliminarDatos.operacionNoEspecificada,
                "monto": $scope.selected.eliminarDatos.monto,
                "tipoOperacion": $scope.selected.eliminarDatos.tipoOperacion,
                "idServicioRealizado": $scope.selected.eliminarDatos.idServicioRealizado,
                "idCaja": $scope.selected.eliminarDatos.idCaja,
                "idOrdenesCompra": $scope.selected.eliminarDatos.idOrdeneenesCompra,
                "idOperaciones" : $scope.selected.eliminarDatos.idOperaciones
            }
            MainFactory.eliminarOperaciones(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        toastr.info('Se eliminó la operación.');
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente.', 'Atención');
                    unBlockUI();
                });


        };

        $scope.agregar = function () {
            $state.go('app.agregar-operacion',{},{reload:true});
        };

    }]);