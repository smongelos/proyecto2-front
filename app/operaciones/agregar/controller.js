app.controller('AgregarOperacionCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};
        $scope.datos.operacionNoEspecificada;
        $scope.datos.monto;
        $scope.datos.tipoOperacion;
        $scope.datos.idServicioRealizado;
        $scope.datos.idCaja;
        $scope.datos.idOrdeneenesCompra;

        /*$scope.formatDate = function (date) {
            var dateobj = date;
            var month = dateobj.getMonth() + 1;
            var day = dateobj.getDate();
            var year = dateobj.getFullYear();
            return day + '/' + month + '/' + year;
        }*/



        $scope.insertar = function () {


            blockUI();
            param = {"estado":true};
            $scope.caja = [];
            MainFactory.listarCajaFiltrada(param)
                .then(function (response) {
                    if (response.status === 200) {
                        console.log('=)')
                        $scope.caja = response.data.lista;
                        console.log($scope.caja.length)
                        $scope.idCaja = $scope.caja[0].idCaja;
                        $scope.estado = $scope.caja[0].estado;
                        if ($scope.estado) {
                            tipo = "operaciones";
                            parametros = {
                                "operacionNoEspecificada": $scope.datos.operacionNoEspecificada,
                                "monto": formatoMonto($scope.datos.monto),
                                "tipoOperacion": $scope.datos.tipoOperacion,
                                "idServicioRealizado": null,
                                "idCaja": $scope.idCaja,
                                "idOrdenesCompra": null,
                                "fecha": null
                            }
                            blockUI();
                            MainFactory.insertarOperaciones(parametros, tipo)
                                .then(function (response) {
                                    if (response.status === 200) {
                                        $scope.agregar = response.data.dato;
                                        toastr.success('Operación registrada con éxito.',"", {
                            "timeOut": "1650"
                        });

                                        $state.go('app.operaciones');
                                    } else {
                                        toastr.info(response.data.mensaje);
                                    }
                                    unBlockUI();
                                }, function (response) {
                                    toastr.info('Ha ocurrido un error, intente nuevamente.', 'Atención');
                                    unBlockUI();
                                });
                        }else {
                            toastr.warning('No hay caja abierta.', 'Atención');
                        }

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente.', 'Atención');
                    unBlockUI();
                });




        }

        $scope.cancelar = function () {
            $state.go('app.operaciones');
        };

        $scope.limpiar = function () {
            $scope.datos.tipoOperacion = "";
            $scope.datos.monto = "";
            $scope.datos.operacionNoEspecificada = "";
        };
        $(document).ready(function(e){
            $('#tipo').focus()
        });        
    }]);