app.controller('ModificarUsuarioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        
        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos
        tipo = 'usuario';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#cambioUsuario').modal('toggle');
        }

        $scope.modificarUsuario = function () {

            var parametros = {
                
                "usuario": $scope.selected.modificarDatos.usuario,
                "contrasenha": $scope.selected.modificarDatos.contrasenha,
                "estado": $scope.selected.modificarDatos.estado,
                "salt": $scope.selected.modificarDatos.salt
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        $state.go('app.usuario');
                    }
                   unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.usuario');
        };
        $(document).ready(function(e){
            $('#pass').focus()
        });
    }]);