app.controller('AgregarUsuarioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
      
        tipos = 'usuario';
        $scope.datos = {};
        $scope.datos.usuario;
        $scope.datos.contrasenha;
        $scope.datos.estado = true;
        $scope.datos.salt;

        $scope.insertar = function () {
            parametros = {
                "usuario": $scope.datos.usuario,
                "contrasenha": $scope.datos.contrasenha,
                "estado": $scope.datos.estado,
                "salt": $scope.datos.contrasenha
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipos)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;

                        $state.go('app.usuario');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.usuario');
        };

        $scope.limpiar = function () {
            $scope.datos.usuario = "";
            $scope.datos.contrasenha = "";
        };
        $(document).ready(function(e){
            $('#usuario').focus()
        });
    }]);