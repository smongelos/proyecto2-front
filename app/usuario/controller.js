app.controller('UsuarioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        

       blockUI();
        $scope.usuario;
        parametros = 'usuario';

        $scope.listarUsuario = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.usuarios = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.usuarios.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.usuarios, params.orderBy()) : $scope.usuarios;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                   unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listarUsuario();



        $scope.modificar = function (usuario) {
            $localStorage.modificarDatos = usuario;
            $state.go('app.modificar-usuario');
        };

        $scope.openModal = function (usuario) {
            $localStorage.eliminarDatos = usuario;
            tipo = $localStorage.eliminarDatos.usuario;
            $('#eliminarUsuario').modal('toggle');
        };

        $scope.eliminar = function () {
            MainFactory.eliminarUsuario(tipo)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó el usuario.');
                        $scope.listarUsuario();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

                $('#eliminarUsuario').modal('toggle');


        };

        $scope.agregar = function () {
            $state.go('app.agregar-usuario');
        };

    }]);