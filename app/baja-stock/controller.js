app.controller('BajaStockCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.baja;
        parametros = 'baja-stock';

        $scope.listar = function () {
            blockUI();
            MainFactory.listaBajaStock()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.baja = response.data.lista;
                        console.log($scope.baja)
                        $scope.tableParams = new NgTableParams({
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.baja.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.baja, params.orderBy()) : $scope.baja;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();

        // $scope.modificar = function (baja) {
        //     $localStorage.modificarDatos = baja;
        //     $state.go('app.modificar-baja-stock');
        // };
       
        $scope.openModal = function (baja) {
            $localStorage.eliminarDatos = baja;
            nroId = $localStorage.eliminarDatos.idBajaStock;
            $('#eliminarBaja').modal('toggle');
        };


        $scope.eliminar = function (baja) {
            console.log($localStorage.eliminarDatos.idBajaStock)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó la baja del stock.',"", {
                            "timeOut": "1650"
                        });
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
            $('#eliminarBaja').modal('hide');


        };

        $scope.agregar = function () {
            $state.go('app.agregar-baja-stock',{},{reload:true});
        };

    }]);