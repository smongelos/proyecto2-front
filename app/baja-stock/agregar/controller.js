app.controller('AgregarBajaStockCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.datos = {};
        $scope.datos.idBajaStock;
        $scope.datos.idEmpleado;
        $scope.datos.idProducto;
        $scope.datos.fecha = new Date();
        $scope.datos.motivoBaja;
        $scope.datos.cantidadBaja;

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.personas = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.producto;
        parametros = 'producto';
        blockUI();
        $scope.producto = [];
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.producto = response.data.lista;
                   
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                unBlockUI();
            });

        $scope.listar();
        $(".opciones").trigger('chosen:updated');

        $scope.insertar = function () {

            tipo = "baja-stock";
            parametros = {
                "idEmpleado": $localStorage.empleado.idEmpleado,
                "idProducto": $scope.datos.idProducto,
                "fecha": $scope.datos.fecha,
                "motivoBaja": $scope.datos.motivoBaja,
                "cantidadBaja": $scope.datos.cantidadBaja

            }
            console.log(parametros)

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        toastr.success('Baja de Stock registrada con éxito.',"", {
                            "timeOut": "1650"
                        });

                        $state.go('app.baja-stock');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.baja-stock');
        };

        $scope.limpiar = function () {
            $scope.datos.idProducto = "";
            $scope.datos.cantidadBaja = "";
            $scope.datos.motivoBaja = "";
        };

        $(document).ready(function(e){
            $('#producto').focus()
        });

 

    }]);