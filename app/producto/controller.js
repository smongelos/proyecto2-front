app.controller('ProductoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.producto;
        parametros = 'producto';

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.producto = response.data.lista;
                        console.log($scope.producto)
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                descripcion: 'asc'
                            },
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.producto.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.producto, params.orderBy()) : $scope.producto;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();




        $scope.modificar = function (producto) {
            $localStorage.modificarDatos = producto;
            $state.go('app.modificar-producto');
        };
       
        $scope.openModal = function (producto) {
            $localStorage.eliminarDatos = producto;
            nroId = $localStorage.eliminarDatos.idProducto;
            $('#eliminarProducto').modal('toggle');
        };


        $scope.eliminar = function (producto) {
            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó el producto.',"", {
                            "timeOut": "1650"
                        });
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
            $('#eliminarProducto').modal('hide');


        };

        $scope.agregar = function () {
            $state.go('app.agregar-producto');
        };

    }]);