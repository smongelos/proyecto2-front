app.controller('AgregarProductoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.datos = {};
        $scope.datos.descripcion;
        $scope.datos.contenido;
        $scope.datos.marca;
        $scope.datos.costo;
        $scope.datos.stockMinimo;
        $scope.datos.stockActual;


        /*$scope.formatDate = function (date) {
            var dateobj = date;
            var month = dateobj.getMonth() + 1;
            var day = dateobj.getDate();
            var year = dateobj.getFullYear();
            return day + '/' + month + '/' + year;
        }*/


        $scope.insertar = function () {
            tipo = "producto";
            parametros = {
                "descripcion": $scope.datos.descripcion,
                "contenido": formatoMonto($scope.datos.contenido),
                "marca": $scope.datos.marca,
                "costo": formatoMonto($scope.datos.costo),
                "stockMinimo": formatoMonto($scope.datos.stockMinimo),
                "stockActual": formatoMonto($scope.datos.stockActual)
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        toastr.success('Producto registrado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.producto');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }

      

        $scope.cancelar = function () {
            $state.go('app.producto');
        };

        $scope.limpiar = function () {
            $scope.datos.descripcion = "";
            $scope.datos.contenido = "";
            $scope.datos.marca = "";
            $scope.datos.costo = "";
            $scope.datos.stockMinimo = "";
            $scope.datos.stockActual = "";
        };
        $(document).ready(function(e){
            $('#descripcion').focus()
        });
    }]);