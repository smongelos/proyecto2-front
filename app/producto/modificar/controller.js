app.controller('ModificarProductoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        $scope.selected.modificarDatos.contenido =  formatear($scope.selected.modificarDatos.contenido);
        $scope.selected.modificarDatos.costo =  formatear($scope.selected.modificarDatos.costo);
        $scope.selected.modificarDatos.stockMinimo =  formatear($scope.selected.modificarDatos.stockMinimo);
        $scope.selected.modificarDatos.stockActual =  formatear($scope.selected.modificarDatos.stockActual);
        console.log($scope.selected.modificarDatos)
        tipo = 'producto';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#cambioProducto').modal('toggle');
        }

        $scope.modificarProducto = function () {

            var parametros = {
                "descripcion": $scope.selected.modificarDatos.descripcion,
                "idProducto": $scope.selected.modificarDatos.idProducto,
                "contenido": formatoMonto($scope.selected.modificarDatos.contenido),
                "costo": formatoMonto($scope.selected.modificarDatos.costo),
                "marca": $scope.selected.modificarDatos.marca,
                "stockMinimo": formatoMonto($scope.selected.modificarDatos.stockMinimo),
                "stockActual": formatoMonto($scope.selected.modificarDatos.stockActual),
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Producto modificado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.producto')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });
        }

        $scope.cancelar = function () {
            $state.go('app.producto');
        };
        $(document).ready(function(e){
            $('#descripcion').focus()
        });
    }]);