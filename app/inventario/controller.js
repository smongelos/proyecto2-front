app.controller('InventarioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        blockUI();
        $scope.inventario;
        parametros = 'inventario';

        $scope.listar = function () {
            blockUI();
            MainFactory.listaInventario()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.inventario = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                idInventario: 'desc'
                            },
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.inventario.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.inventario, params.orderBy()) : $scope.inventario;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }

        $scope.listar();

        $scope.modificar = function (inventario) {
            $localStorage.modificarDatos = inventario;
            $state.go('app.modificar-inventario');
        };

        $scope.eliminar = function (inventario) {
            $localStorage.eliminarDatos = inventario;
             nroId = $localStorage.eliminarDatos.idInventario;
             tipo = 'inventario'
 
             MainFactory.eliminarDatos(tipo, nroId)
                 .then(function (response) {
                     if (response.status === 204) {
                         toastr.success('Se eliminó el inventario.');
                         $scope.listar();
                     } else {
                         toastr.info(response.data.mensaje);
                     }
                     unBlockUI();
                 }, function (response) {
                     toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                     unBlockUI();
                 }); 
         };

        $scope.agregar = function () {
            tipo = "inventario";
            parametros = {
                "idEmpleado" : $localStorage.empleado.idEmpleado,
                "fecha": new Date()
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        $state.reload();
                        toastr.success('Inventario registrado con éxito.',"", {
                            "timeOut": "1650"
                        });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }

        $scope.verMas = function (inventario) {
            $localStorage.datos = inventario;
            $state.go('app.inventario-detalles');
        };        
    }]);