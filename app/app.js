var app = angular.module('ng-scrum', ['ui.router', 'ngTable', 'ngStorage', 'ngBootbox', 'ngMaterial', 'ngMessages'])
    .config(function ($stateProvider, $httpProvider, $urlRouterProvider, $qProvider) {
        $stateProvider
            .state('login', {
                url: '/login',
                cache: false,
                templateUrl: 'app/login/view.html',
                controller: 'LoginCtrl',
                module: 'public'
            })
            .state('app', {
                url: '/app',
                cache: false,
                abstract: true,
                templateUrl: 'app/menu/menu.html',
                controller: 'MenuCtrl',
                module: 'private'
            })
            .state('app.inicio', {
                url: '/inicio',
                templateUrl: 'app/inicio/inicio.html',
                controller: 'InicioCtrl',
                module: 'private'
            })
            .state('app.administracion', {
                url: '/administracion',
                templateUrl: 'app/administrador/view.html',
                controller: 'AdministracionCtrl',
                module: 'private'
            })
            .state('app.usuario', {
                url: '/usuario',
                templateUrl: 'app/usuario/view.html',
                controller: 'UsuarioCtrl',
                module: 'private'
            })
            .state('app.modificar-usuario', {
                url: '/modificar-usuario',
                templateUrl: 'app/usuario/modificar/view.html',
                controller: 'ModificarUsuarioCtrl',
                module: 'private'
            })   
            .state('app.agregar-usuario', {
                url: '/agregar-usuario',
                templateUrl: 'app/usuario/agregar/view.html',
                controller: 'AgregarUsuarioCtrl',
                module: 'private'
            }) 
            .state('app.cliente', {
                url: '/cliente',
                templateUrl: 'app/cliente/view.html',
                controller: 'ClienteCtrl',
                module: 'private'
            })    
            .state('app.agregar-cliente', {
                url: '/agregar-cliente',
                templateUrl: 'app/cliente/agregar/view.html',
                controller: 'AgregarClienteCtrl',
                module: 'private'
            })   
            .state('app.modificar-cliente', {
                url: '/modificar-cliente',
                templateUrl: 'app/cliente/modificar/view.html',
                controller: 'ModificarClienteCtrl',
                module: 'private'
            })
            .state('app.persona', {
                url: '/persona',
                templateUrl: 'app/persona/view.html',
                controller: 'PersonaCtrl',
                module: 'private'
            })   
            .state('app.agregar-persona', {
                url: '/agregar-persona',
                templateUrl: 'app/persona/agregar/view.html',
                controller: 'AgregarPersonaCtrl',
                module: 'private'
            })   
            .state('app.modificar-persona', {
                url: '/modificar-persona',
                templateUrl: 'app/persona/modificar/view.html',
                controller: 'ModificarPersonaCtrl',
                module: 'private'
            })
            .state('app.proveedor', {
                url: '/proveedor',
                templateUrl: 'app/proveedor/view.html',
                controller: 'ProveedorCtrl',
                module: 'private'
            })   
            .state('app.agregar-proveedor', {
                url: '/agregar-proveedor',
                templateUrl: 'app/proveedor/agregar/view.html',
                controller: 'AgregarProveedorCtrl',
                module: 'private'
            })   
            .state('app.modificar-proveedor', {
                url: '/modificar-proveedor',
                templateUrl: 'app/proveedor/modificar/view.html',
                controller: 'ModificarProveedorCtrl',
                module: 'private'
            })
            .state('app.estado', {
                url: '/estado',
                templateUrl: 'app/estado/view.html',
                controller: 'EstadoCtrl',
                module: 'private'
            })   
            .state('app.agregar-estado', {
                url: '/agregar-estado',
                templateUrl: 'app/estado/agregar/view.html',
                controller: 'AgregarEstadoCtrl',
                module: 'private'
            })   
            .state('app.modificar-estado', {
                url: '/modificar-estado',
                templateUrl: 'app/estado/modificar/view.html',
                controller: 'ModificarEstadoCtrl',
                module: 'private'
            })
            .state('app.rubro', {
                url: '/rubro',
                templateUrl: 'app/rubro/view.html',
                controller: 'RubroCtrl',
                module: 'private'
            })   
            .state('app.agregar-rubro', {
                url: '/agregar-rubro',
                templateUrl: 'app/rubro/agregar/view.html',
                controller: 'AgregarRubroCtrl',
                module: 'private'
            })   
            .state('app.modificar-rubro', {
                url: '/modificar-rubro',
                templateUrl: 'app/rubro/modificar/view.html',
                controller: 'ModificarRubroCtrl',
                module: 'private'
            })
            .state('app.producto', {
                url: '/producto',
                templateUrl: 'app/producto/view.html',
                controller: 'ProductoCtrl',
                module: 'private'
            })   
            .state('app.agregar-producto', {
                url: '/agregar-producto',
                templateUrl: 'app/producto/agregar/view.html',
                controller: 'AgregarProductoCtrl',
                module: 'private'
            })   
            .state('app.modificar-producto', {
                url: '/modificar-producto',
                templateUrl: 'app/producto/modificar/view.html',
                controller: 'ModificarProductoCtrl',
                module: 'private'
            })  
            .state('app.empleado', {
                url: '/empleado',
                templateUrl: 'app/empleado/view.html',
                controller: 'EmpleadoCtrl',
                module: 'private'
            })  
            .state('app.agregar-empleado', {
                url: '/agregar-empleado',
                templateUrl: 'app/empleado/agregar/view.html',
                controller: 'AgregarEmpledoCtrl',
                module: 'private'
            }) 
            .state('app.modificar-empleado', {
                url: '/modificar-empleado',
                templateUrl: 'app/empleado/modificar/view.html',
                controller: 'ModificarEmpleadoCtrl',
                module: 'private'
            }) 
            .state('app.empleado-detalle', {
                url: '/empleado-detalle',
                templateUrl: 'app/empleado-detalle/view.html',
                controller: 'EmpleadoDetalleCtrl',
                module: 'private'
            })  
            .state('app.modificar-empleado-detalle', {
                url: '/modificar-empleado-detalle',
                templateUrl: 'app/empleado-detalle/modificar/view.html',
                controller: 'ModificarEmpleadoDetalleCtrl',
                module: 'private'
            }) 
            .state('app.agregar-empleado-detalle', {
                url: '/agregar-empleado-detalle',
                templateUrl: 'app/empleado-detalle/agregar/view.html',
                controller: 'AgregarEmpledoDetalleCtrl',
                module: 'private'
            })
            .state('app.servicio', {
                url: '/servicio',
                templateUrl: 'app/servicio/view.html',
                controller: 'ServicioCtrl',
                module: 'private'
            })  
            .state('app.agregar-servicio', {
                url: '/agregar-servicio',
                templateUrl: 'app/servicio/agregar/view.html',
                controller: 'AgregarServicioCtrl',
                module: 'private'
            }) 
            .state('app.modificar-servicio', {
                url: '/modificar-servicio',
                templateUrl: 'app/servicio/modificar/view.html',
                controller: 'ModificarServicioCtrl',
                module: 'private'
            })
            .state('app.servicio-producto', {
                url: '/servicio-producto',
                templateUrl: 'app/servicio-producto/view.html',
                controller: 'ServicioProductoCtrl',
                module: 'private'
            })  
            .state('app.modificar-servicio-producto', {
                url: '/modificar-servicio-producto',
                templateUrl: 'app/servicio-producto/modificar/view.html',
                controller: 'ModificarServicioProductoCtrl',
                module: 'private'
            }) 
            .state('app.agregar-servicio-producto', {
                url: '/agregar-servicio-producto',
                templateUrl: 'app/servicio-producto/agregar/view.html',
                controller: 'AgregarServicioProductoCtrl',
                module: 'private'
            })
            .state('app.operaciones', {
                url: '/operaciones',
                templateUrl: 'app/operaciones/view.html',
                controller: 'OperacionesCtrl',
                module: 'private'
            }) 
            .state('app.agregar-operacion', {
                url: '/agregar-operacion',
                templateUrl: 'app/operaciones/agregar/view.html',
                controller: 'AgregarOperacionCtrl',
                module: 'private'
            })
            .state('app.modificar-operacion', {
                url: '/modificar-operacion',
                templateUrl: 'app/operaciones/modificar/view.html',
                controller: 'ModificarOperacionCtrl',
                module: 'private'
            }) 
            .state('app.caja', {
                url: '/caja',
                templateUrl: 'app/caja/view.html',
                controller: 'CajaCtrl',
            })
             
            .state('app.agregar-caja', {
                url: '/agregar-caja',
                templateUrl: 'app/caja/agregar/view.html',
                controller: 'AgregarCajaCtrl',
                module: 'private'
            })
            
            .state('app.modificar-caja', {
                url: '/modificar-caja',
                templateUrl: 'app/caja/modificar/view.html',
                controller: 'ModificarCajaCtrl',
                module: 'private'
            })
            /* SERVICIO REALIZADO */
            .state('app.servicio-realizado', {
                url: '/servicio-realizado',
                templateUrl: 'app/servicio-realizado/view.html',
                controller: 'ServicioRealizadoCtrl',
                module: 'private'
            })  
            .state('app.agregar-servicio-realizado', {
                url: '/agregar-servicio-realizado',
                templateUrl: 'app/servicio-realizado/agregar/view.html',
                controller: 'AgregarServicioRealizadoCtrl',
                module: 'private'
            }) 
            .state('app.modificar-servicio-realizado', {
                url: '/modificar-servicio-realizado',
                templateUrl: 'app/servicio-realizado/modificar/view.html',
                controller: 'ModificarServicioRealizadoCtrl',
                module: 'private'
            }) 
            /* SERVICIO REALIZADO DETALLES */
            .state('app.servicio-realizado-detalles', {
                url: '/servicio-realizado-detalles',
                templateUrl: 'app/servicio-realizado-detalles/view.html',
                controller: 'ServicioRealizadoDetallesCtrl',
                module: 'private'
            })  
            .state('app.modificar-servicio-realizado-detalles', {
                url: '/modificar-servicio-realizado-detalles',
                templateUrl: 'app/servicio-realizado-detalles/modificar/view.html',
                controller: 'ModificarServicioRealizadoDetallesCtrl',
                module: 'private'
            }) 
            .state('app.agregar-servicio-realizado-detalles', {
                url: '/agregar-servicio-realizado-detalles',
                templateUrl: 'app/servicio-realizado-detalles/agregar/view.html',
                controller: 'AgregarServicioRealizadoDetallesCtrl',
                module: 'private'
            })
            /* COBRANZA */
            .state('app.cobranza', {
                url: '/cobranza',
                templateUrl: 'app/cobranza/view.html',
                controller: 'CobranzaCtrl',
                module: 'private'
            })
            /* COBRANZA DETALLES */
            .state('app.cobranza-detalles', {
                url: '/cobranza-detalles',
                templateUrl: 'app/cobranza-detalles/view.html',
                controller: 'CobranzaDetallesCtrl',
                module: 'private'
            })
            .state('app.baja-stock', {
                url: '/baja-stock',
                templateUrl: 'app/baja-stock/view.html',
                controller: 'BajaStockCtrl',
            })
            .state('app.agregar-baja-stock', {
                url: '/agregar-baja-stock',
                templateUrl: 'app/baja-stock/agregar/view.html',
                controller: 'AgregarBajaStockCtrl',
                module: 'private'
            })
            // .state('app.modificar-servicio-realizado', {
            //     url: '/modificar-servicio-realizado',
            //     templateUrl: 'app/servicio-realizado/modificar/view.html',
            //     controller: 'ModificarServicioRealizadoCtrl',
            //     module: 'private'
            // })
            .state('app.inventario', {
                url: '/inventario',
                templateUrl: 'app/inventario/view.html',
                controller: 'InventarioCtrl',
                module: 'private'
            })
            .state('app.inventario-detalles', {
                url: '/inventario-detalles',
                templateUrl: 'app/inventario-detalles/view.html',
                controller: 'InventarioDetallesCtrl',
                module: 'private'
            })
            .state('app.orden-compra', {
                url: '/orden-compra',
                templateUrl: 'app/orden-compra/view.html',
                controller: 'OrdenCompraCtrl',
                module: 'private'
            })  
            .state('app.agregar-orden-compra', {
                url: '/agregar-orden-compra',
                templateUrl: 'app/orden-compra/agregar/view.html',
                controller: 'AgregarOrdenCompraCtrl',
                module: 'private'
            })
            .state('app.orden-compra-detalles', {
                url: '/orden-compra-detalles',
                templateUrl: 'app/orden-compra-detalles/view.html',
                controller: 'OrdenCompraDetallesCtrl',
                module: 'private'
            })
            .state('app.agregar-orden-compra-detalles', {
                url: '/agregar-orden-compra-detalles',
                templateUrl: 'app/orden-compra-detalles/agregar/view.html',
                controller: 'AgregarOrdenCompraDetallesCtrl',
                module: 'private'
            })
            .state('app.confirmar-orden-compra', {
                url: '/confirmar-orden-compra',
                templateUrl: 'app/orden-compra-detalles/confirmar/view.html',
                controller: 'ConfirmarRecepcionOrdenCtrl',
                module: 'private'
            })
            .state('app.modificar-confimacion-orden', {
                url: '/modificar-confimacion-orden',
                templateUrl: 'app/orden-compra-detalles/confirmar/modificar/view.html',
                controller: 'ModificarConfirmacionOrdenCtrl',
                module: 'private'
            })
            .state('app.factura', {
                url: '/factura',
                templateUrl: 'app/facturas/view.html',
                controller: 'FacturaCtrl',
                module: 'private'
            })
            ;

        $urlRouterProvider.otherwise('/login');
        $qProvider.errorOnUnhandledRejections(false);


        //            var sessionInterceptor = ['$location', '$rootScope', '$injector', function ($location, $rootScope, $injector) {
        //                    return {
        //                        'response': function (resp) {
        //                            $rootScope.valor = $location.path();
        //                            if (resp.status == 401) {
        //                                $.ajax({
        //                                    type: 'POST',
        //                                    url: contexto + '/rest/sesion/cerrar',
        //                                    success: function (data) {
        //                                        console.log(data);
        //                                        window.location.href = '#login';
        //                                        window.location.reload();
        //                                    }
        //                                });
        //                            }
        //                            ;
        //                            return resp;
        //                        },
        //                        'responseError': function (resp) {
        //                            if (resp.status == 401) {
        //                                $.ajax({
        //                                    type: 'POST',
        //                                    url: contexto + '/rest/sesion/cerrar',
        //                                    success: function (data) {
        //                                        console.log(data);
        //                                        window.location.href = '#login';
        //                                        window.location.reload();
        //                                    }
        //                                });
        //                            }
        //                            ;
        //                            return resp;
        //                        }
        //                    };
        //                }];
        //
        //            $httpProvider.interceptors.push(sessionInterceptor);

    });

