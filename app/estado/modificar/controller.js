app.controller('ModificarEstadoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        console.log($scope.selected.modificarDatos)
        tipo = 'estados';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            console.log('hola')
            $('#cambioEstado').modal('toggle');
        }

        $scope.modificarEstado = function () {

            var parametros = {
                "descripcion": $scope.selected.modificarDatos.descripcion,
                "idEstado": $scope.selected.modificarDatos.idEstado,
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Estado modificado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.estado')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });
        }

        $scope.cancelar = function () {
            $state.go('app.estado');
        };
    }]);