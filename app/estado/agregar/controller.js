app.controller('AgregarEstadoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};
        $scope.datos.descripcion;

        /*$scope.formatDate = function (date) {
            var dateobj = date;
            var month = dateobj.getMonth() + 1;
            var day = dateobj.getDate();
            var year = dateobj.getFullYear();
            return day + '/' + month + '/' + year;
        }*/


        $scope.insertar = function () {
            tipo = "estados";
            parametros = {
                "descripcion": $scope.datos.descripcion,
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        toastr.success('Estado registrado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.estado');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }

      

        $scope.cancelar = function () {
            $state.go('app.estado');
        };

        $scope.limpiar = function () {
            $scope.datos.descripcion = "";
        };
    }]);