app.controller('EstadoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.estado;
        parametros = 'estados';

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.estado = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                descripcion: 'asc'
                            },
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.estado.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.estado, params.orderBy()) : $scope.estado;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();



        $scope.modificar = function (estado) {
            $localStorage.modificarDatos = estado;
            $state.go('app.modificar-estado');
        };

        $scope.modalShown = false;
        $scope.eliminarDatos;
        
        $scope.openModal = function (estado) {
            $localStorage.eliminarDatos = estado;
            nroId = $localStorage.eliminarDatos.idEstado;
            $('#eliminarEstado').modal('toggle');
        };

        $scope.eliminar = function (estado) {
            console.log($localStorage.eliminarDatos.idEstado)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó el estado.');
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
            $('#eliminarEstado').modal('hide');


        };

    $scope.agregar = function () {
            $state.go('app.agregar-estado');
        };
    }]);