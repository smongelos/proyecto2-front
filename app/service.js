app.factory('MainFactory', ['$http', function ($http) {
    return {
        verificarLogin: function (param) {
            var urlEncondeURIComponent = encodeURIComponent(angular.toJson(param));
            return $http.get( 'bella-utopia/rest/vista-proyecto/listar-usuario?contrasenha='+param.contrasenha+'&usuario='+param.usuario);
        },
        listar: function (parametros) {
            return $http.get(`bella-utopia/rest/${parametros}/listar?cantidad=-1`);
        },
        listaCliente: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-clientes`);
        },
        agregarDatos: function (parametros, tipo) {
            return $http.post(`bella-utopia/rest/${tipo}/insertar`, parametros);
        },
        modificarDatosActuales: function (parametros, tipo) {
            return $http.put(`bella-utopia/rest/${tipo}/modificar`, parametros);
        },
        eliminarDatos: function (tipo, nroId) {
            return $http.delete(`bella-utopia/rest/${tipo}/${nroId}`);
        },
        
        listaEmpleado: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-empleado`);
        },

        listaEmpleadoDetalles: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-empleado-detalles`);
        },        
        listaServicioRealizado: function (idServicioRealizado,  pagado) {
            var param = "";
            var first = true;
            if(idServicioRealizado != null && idServicioRealizado != '' && typeof idServicioRealizado != 'undefined'){
                first = false;
                param += `?idServicioRealizado=${idServicioRealizado}` ;
            }
            if(pagado != null && pagado != '' && typeof pagado != 'undefined'){
                if(first){
                    first = false;
                    param += "?";
                }else{
                    param += "&"
                }
                param += `pagado=${pagado}` ;
            }
            
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-servicio-realizado${param}`);
        },

        eliminarUsuario: function () {
            return $http.delete(`bella-utopia/rest/usuario/eliminar/${tipo}`);
        },

        listaServicio: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-servicio`);
        },

        listaServicioProducto: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-servicio-producto`);
        },

        insertarOperaciones: function (parametros, tipo) {
            return $http.post(`bella-utopia/rest/${tipo}/insertar-operaciones`, parametros);
        },

        eliminarOperaciones: function (parametros, tipo) {
            return $http.post(`bella-utopia/rest/operaciones/eliminar-operaciones`, parametros);
        },
        
        listarCajaFiltrada: function (param) {
            var urlEncondeURIComponent = encodeURIComponent(angular.toJson(param));
            return $http.get( 'bella-utopia/rest/caja/listar?filtros='+urlEncondeURIComponent);
        },
        
        listarCaja: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-caja`);
        },

        listarServicioRealizadoDetalles: function (idServicioRealizado) {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-servicio-realizado-detalles?idServicioRealizado=${idServicioRealizado}`);
        },

        insertarServicioRealizadoDetalles: function (parametros, tipo) {
            return $http.post(`bella-utopia/rest/${tipo}/insertar-servicio-realizado-detalles`, parametros);
        },
        eliminarServicioRealizadoDetales: function (parametros, tipo) {
            return $http.post(`bella-utopia/rest/${tipo}/eliminar-servicio-realizado-detalles`, parametros);
        },

        listaInventario: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-inventario`);
        },
        listaInventarioDetalles: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-inventario-detalles`);
        },        
        pagarServicioRealizado: function (parametros) {
            return $http.post(`bella-utopia/rest/servicio-realizado/realizar-pago`, parametros);
        },
        obtenerServicioRealizado: function (id) {
            return $http.get(`bella-utopia/rest/servicio-realizado/obtener?id=${id}`);
        },
        listaBajaStock: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-baja-stock`);
        },
        listaOrdenCompra: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-orden-compra`);
        },
        listaOrdenCompraDetalles: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-orden-compra-detalles`);
        },
        listaFactura: function () {
            return $http.get(`bella-utopia/rest/vista-proyecto/listar-facturas`);
        },

    };
}]);