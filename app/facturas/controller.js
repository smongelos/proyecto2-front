app.controller('FacturaCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();

        $scope.listar = function () {
            blockUI();
            MainFactory.listaFactura()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.fact = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                fecha: 'desc'
                            },
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.fact.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.fact, params.orderBy()) : $scope.fact;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();

        
        $scope.imprimir = function(item){

            var url = 'bella-utopia/rest/reportes/facturacion?facturaId='+ item.idFactura;

            $(document.body).append($('<form target="_blank" id="tmpDownloadForm" method="post" action="' + url
                + '" style="display:none"></form>'));
            $('#tmpDownloadForm').submit().remove();
        };


    }]);