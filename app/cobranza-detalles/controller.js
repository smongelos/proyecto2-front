app.controller('CobranzaDetallesCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.selected = {};
        $scope.selected.datos = $localStorage.datos;
        $scope.idServicioRealizado = $scope.selected.datos.idServicioRealizado;
        $scope.listaFiltro = [];
        tipo = "servicio-realizado-detalles";

        $scope.obtenerServicioRealizado = function () {
            blockUI();
            MainFactory.listaServicioRealizado($scope.idServicioRealizado)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.selected.datos = response.data.lista[0];
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar = function () {
            blockUI();
            MainFactory.listarServicioRealizadoDetalles($scope.idServicioRealizado)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.listaFiltro = response.data.lista;
                        if ($scope.listaFiltro.length > 0) {
                            $scope.tableParams = new NgTableParams({
                                page: 1,
                                count: 9,
                            }, {
                                    counts: [],
                                    total: $scope.listaFiltro.length,
                                    getData: function (params) {
                                        var orderedData = params.sorting() ? $filter('orderBy')($scope.listaFiltro, params.orderBy()) : $scope.proyectoAsignado;
                                        return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                    }
                                });
                        }

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();
        $scope.obtenerServicioRealizado();



        $scope.modificar = function (detalle) {
            $localStorage.modificarDatos = detalle;
            $state.go('app.modificar-servicio-realizado-detalles');
        };

        $scope.eliminar = function (detalle) {
            if ($scope.selected.datos.pagado) {
                toastr.warning('El servicio ya se encuentra pagado y cerrado');
                return;
            }
            var param = {
                "idServicio": detalle.idServicio,
                "idServicioRealizado": detalle.idServicioRealizado,
                "idServicioRealizadoDetalles": detalle.idServicioRealizadoDetalles,
                /* "idEmpleado": $scope.selected.servicioRealizado.idEmpleado */
                "idEmpleado": detalle.idEmpleado
            }

            MainFactory.eliminarServicioRealizadoDetales(param, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        toastr.success('Se eliminó el servicio realizado detalle');
                        $scope.listar();
                        $scope.obtenerServicioRealizado();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });


        };

        $scope.agregar = function (servicioRealizado) {
            if ($scope.selected.datos.pagado) {
                toastr.warning('El servicio ya se encuentra pagado y cerrado');
                return;
            }
            $localStorage.servicioRealizado = servicioRealizado;
            $state.go('app.agregar-servicio-realizado-detalles');
        };


        $scope.pagar = function (servicioRealizado) {
            param = {"estado":true};
            $scope.caja = [];
            MainFactory.listarCajaFiltrada(param)
                .then(function (response) {
                    if (response.status === 200) {
                        if(!$scope.verificarCajas(response.data.lista)){
                            toastr.warning('No se encuentra ninguna caja abierta', 'Atención');
                            return;
                        }
                        
                        $scope.caja = response.data.lista[0];
                        $scope.idCaja = $scope.caja.idCaja;
                        let servicioRealizado = {
                            "idServicioRealizado": $scope.selected.datos.idServicioRealizado,
                            "montoTotal": $scope.selected.datos.montoTotal,
                            "idCliente": $scope.selected.datos.idCliente,
                            "pagado": $scope.selected.datos.pagado

                        }
                        var cobranzaParam = {
                            "servicioRealizado": servicioRealizado,
                            "caja": $scope.caja
                        }
                        MainFactory.pagarServicioRealizado(cobranzaParam)
                        .then(function (response) {
                            if (response.status === 200) {
        
                                toastr.success("Servicio pagado exitosamente.","", {
                                    "timeOut": "1650"
                                });
                                $state.go('app.cobranza');
        
                            } else {
                                toastr.info(response.data.mensaje);
                            }
                            unBlockUI();
                        }, function (response) {
                            toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                            "timeOut": "1650"
                    });
                            unBlockUI();
                        });

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        };

        $scope.parseDate =function(respuesta){
            return respuesta.getFullYear() + '-' + (respuesta.getMonth() +  1) + '-' + respuesta.getDate()
        }

        
        $scope.verificarCajas = function (listaCajas) {
            for(caja in listaCajas){
                if(listaCajas[caja].estado){
                    return true;
                }
            }
            return false;
        };
          
    }]);