app.controller('AgregarEmpledoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};

        $scope.datos.idPersona;
        $scope.datos.fechaIngreso = new Date();
        $scope.datos.fechaSalida;

        $scope.listar = function () {
            parametros = 'persona'
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.personas = response.data.lista;

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                // $(".opciones").trigger('chosen:updated');
                });

            blockUI();
            parametros = 'usuario'
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.usuarios = response.data.lista;

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                            "timeOut": "1650"
                        });
                    unBlockUI();
                // $(".opciones").trigger('chosen:updated');
                });
        }


        $scope.listar();
        $(".opciones").trigger('chosen:updated');

        $scope.insertar = function () {

            tipo = "empleado";
            parametros = {
                "usuario": $scope.datos.usuario,
                "fechaIngreso": $scope.datos.fechaIngreso,
                "fechaSalida": $scope.datos.fechaSalida,
                "idPersona": $scope.datos.idPersona
            }

            console.log(parametros)

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        toastr.success('Empleado registrado con éxito.',"", {
                            "timeOut": "1650"
                        });

                        $state.go('app.empleado');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }

        $scope.cancelar = function () {
            $state.go('app.empleado');
        };

        $scope.limpiar = function () {
            $scope.datos.usuario = "";
            $scope.datos.fechaIngreso = "";
            $scope.datos.fechaSalida = "";
            $scope.datos.idPersona = "";
        };
        $(document).ready(function(e){
            $('#nombre').focus()
        });
    }]);