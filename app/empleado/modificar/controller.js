app.controller('ModificarEmpleadoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        $scope.selected.modificarDatos.fecha_ingreso = new Date(+new Date($scope.selected.modificarDatos.fecha_ingreso) + 4 * 60 * 60 * 1000);

        if ($scope.selected.modificarDatos.fecha_salida === null){
            $scope.selected.modificarDatos.fecha_salida = new Date();
        } else{
            $scope.selected.modificarDatos.fecha_salida = new Date(+new Date($scope.selected.modificarDatos.fecha_salida) + 4 * 60 * 60 * 1000);
        };

        console.log($scope.selected.modificarDatos)
        tipo = 'empleado';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            console.log('hola')
            $('#cambioEmpleado').modal('toggle');
        }

        $scope.modificarPersona = function () {

            var parametros = {
                "fechaSalida": $scope.selected.modificarDatos.fecha_salida,
                "usuario": $scope.selected.modificarDatos.usuario,
                "fechaIngreso": $scope.selected.modificarDatos.fecha_ingreso,
                "idEmpleado": $scope.selected.modificarDatos.id_empleado,
                "idPersona": $scope.selected.modificarDatos.id_persona
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Empleado modificado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.empleado')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.empleado');
        };
        $(document).ready(function(e){
            $('#fechaIngreso').focus()
        });
    }]);