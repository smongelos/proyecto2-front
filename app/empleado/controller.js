app.controller('EmpleadoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.empleados;
        parametros = 'empleado';

        $scope.listar = function () {
            blockUI();
            MainFactory.listaEmpleado()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.empleados = response.data.lista;
                        console.log($scope.empleados)
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                nombre: 'asc'
                            },    
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.empleados.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.empleados, params.orderBy()) : $scope.empleados;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();



        $scope.modificar = function (empleados) {
            $localStorage.modificarDatos = empleados;
            $state.go('app.modificar-empleado');
        };

        $scope.openModal = function (empleados) {
            $localStorage.eliminarDatos = empleados;
             nroId = $localStorage.eliminarDatos.id_empleado;
            $('#eliminarEmpleado').modal('toggle');
        };


        $scope.eliminar = function (empleados) {
             tipo = 'empleado'
 
             MainFactory.eliminarDatos(tipo, nroId)
                 .then(function (response) {
                     if (response.status === 204) {
                         toastr.success('Se eliminó el empleado.');
                         $scope.listar();
                     } else {
                         toastr.info(response.data.mensaje);
                     }
                     unBlockUI();
                 }, function (response) {
                     toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                     unBlockUI();
                 });

                 $('#eliminarEmpleado').modal('toggle');
 
 
         };

        $scope.agregar = function () {
            $state.go('app.agregar-empleado',{},{reload:true});

        };

        $scope.verMas = function (empleados) {
            $localStorage.datos = empleados;
            $state.go('app.empleado-detalle');
        };

    }]);