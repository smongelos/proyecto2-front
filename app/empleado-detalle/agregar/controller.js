app.controller('AgregarEmpledoDetalleCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.detalleEmplado = $localStorage.detalleEmplado;
        console.log($scope.selected.detalleEmplado)
        $scope.datos = {};
        $scope.datos.fechaAsignacion = new Date();
        $scope.datos.idEmpleadoDetalles;
        $scope.datos.idEmpleado;
         $scope.datos.idRubro;
        $scope.datos.salario;

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.personas = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.rubros;
        parametros = 'rubro';
        blockUI();
        $scope.rubros = [];
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.rubros = response.data.lista;
                   
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                unBlockUI();
            });

        $scope.listar();
        $(".opciones").trigger('chosen:updated');

        $scope.insertar = function () {

            tipo = "empleado-detalles";
            parametros = {
                "fechaAsignacion": $scope.datos.fechaAsignacion,
                "idEmpleado": $scope.selected.detalleEmplado.id_empleado,
                "idRubro": $scope.datos.idRubro,
                "salario": formatoMonto($scope.datos.salario)
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        toastr.success('Salario y Rubro registrados exitosamente.',"", {
                            "timeOut": "1650"
                        });
                        $scope.agregar = response.data.dato;

                        $state.go('app.empleado-detalle');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.empleado-detalle');
        };

        $scope.limpiar = function () {
            $scope.datos.fechaAsignacion= "";
            $scope.selected.detalleEmplado.id_empleado= "";
             $scope.datos = {};
        };
        $(document).ready(function(e){
            $('#rubro').focus()
        }); 
    }]);