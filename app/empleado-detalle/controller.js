app.controller('EmpleadoDetalleCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.selected = {};
        $scope.selected.datos = $localStorage.datos;
        console.log($scope.selected.datos)
        $scope.detalle;
        $scope.listaFiltro = [];
        parametros = 'persona';

        $scope.listar = function () {
            blockUI();
            MainFactory.listaEmpleadoDetalles()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.detalle = response.data.lista;
                        $scope.auxi = $scope.selected.datos;
                        for (var i in $scope.detalle) {
                            if ($scope.detalle[i].idEmpleado == $scope.auxi.id_empleado) {
                                $scope.listaFiltro.push($scope.detalle[i]);
                            }
                        }
                        if ($scope.listaFiltro.length > 0) {
                            console.log($scope.listaFiltro);
                            $scope.tableParams = new NgTableParams({
                                sorting: {
                                    fecha_asignacion: 'desc'
                                },
                                page: 1,
                                count: 9,
                            }, {
                                    counts: [],
                                    total: $scope.listaFiltro.length,
                                    getData: function (params) {
                                        var orderedData = params.sorting() ? $filter('orderBy')($scope.listaFiltro, params.orderBy()) : $scope.proyectoAsignado;
                                        return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                    }
                                });
                        }

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();



        $scope.modificar = function (detalleEmplado) {
            $localStorage.modificarDatos = detalleEmplado;
            $state.go('app.modificar-empleado-detalle');
        };

        $scope.eliminar = function (detalle) {
            parametros = 'empleado-detalles';
            $localStorage.eliminarDatos = detalle;
            nroId = $localStorage.eliminarDatos.idEmpleadoDetalles;
            console.log($localStorage.eliminarDatos.idEmpleadoDetalles)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó el detalle del empleado.',"", {
                            "timeOut": "1650"
                        });
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        $state.reload();
        };

        $scope.agregar = function (detalleEmplado) {
            $localStorage.detalleEmplado = detalleEmplado;
            $state.go('app.agregar-empleado-detalle',{},{reload:true});
        };

    }]);