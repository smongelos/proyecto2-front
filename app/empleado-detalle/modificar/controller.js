app.controller('ModificarEmpleadoDetalleCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        console.log($scope.selected.modificarDatos);
        $scope.selected.modificarDatos.fecha_asignacion = new Date(+new Date($scope.selected.modificarDatos.fecha_asignacion) + 4 * 60 * 60 * 1000);
        $scope.selected.modificarDatos.salario =  formatear($scope.selected.modificarDatos.salario);
        tipo = 'empleado-detalles';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            console.log('hola')
            $('#cambioEmpleado').modal('toggle');
        }

        $scope.rubros;
        parametros = 'rubro';
        blockUI();
        $scope.rubros = [];
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.rubros = response.data.lista;
                   
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                unBlockUI();
            });

        $scope.modificarPersona = function () {

            var parametros = {

                "idEmpleadoDetalles": $scope.selected.modificarDatos.idEmpleadoDetalles,
                "fechaAsignacion": $scope.selected.modificarDatos.fecha_asignacion,
                "idEmpleado": $scope.selected.modificarDatos.idEmpleado,
                "idRubro": $scope.selected.modificarDatos.idRubro,
                "salario": formatoMonto($scope.selected.modificarDatos.salario)
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Detalle Empleado modificado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.empleado-detalle')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.empleado-detalle');
        };
        $(document).ready(function(e){
            $('#idRubro').focus()
        });
    }]);