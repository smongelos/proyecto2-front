app.controller('AgregarServicioRealizadoDetallesCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.datos = {};
        $scope.selected = {};
        $scope.selected.servicioRealizado = $localStorage.servicioRealizado;
        console.log($scope.selected.detalleEservicioRealizadomplado)

        parametros = 'servicio';

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.servicios = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        blockUI();
            MainFactory.listaEmpleado()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.empleado = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        $scope.listar();
        $(".opciones").trigger('chosen:updated');
        
        $scope.insertar = function () {

            tipo = "servicio-realizado-detalles";
            parametros = {
                "idServicio": $scope.datos.idServicio,
                "idServicioRealizado": $scope.selected.servicioRealizado.idServicioRealizado,
                /* "idEmpleado": $scope.selected.servicioRealizado.idEmpleado */
                "idEmpleado": $scope.datos.id_empleado
            }

            blockUI();
            MainFactory.insertarServicioRealizadoDetalles(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        $state.go('app.servicio-realizado-detalles');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning(response.data.messages, 'Error');
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.servicio-realizado-detalles');
        };

        $scope.limpiar = function () {
            $scope.datos.idServicio = "";
        };
        $(document).ready(function(e){
            $('#empleado').focus()
        });

 

 
    }]);