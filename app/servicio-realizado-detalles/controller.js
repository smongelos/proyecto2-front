app.controller('ServicioRealizadoDetallesCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.selected = {};
        $scope.selected.datos = $localStorage.datos;
        $scope.idServicioRealizado = $scope.selected.datos.idServicioRealizado;
        $scope.listaFiltro = [];
        tipo = "servicio-realizado-detalles";

        $scope.obtenerServicioRealizado = function () {
            blockUI();
            MainFactory.listaServicioRealizado($scope.idServicioRealizado)
                .then(function (response) {
                    if (response.status === 200) {
                        console.log(response.data.lista[0])
                        $scope.selected.datos = response.data.lista[0];
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar = function () {
            blockUI();
            MainFactory.listarServicioRealizadoDetalles($scope.idServicioRealizado)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.listaFiltro = response.data.lista;
                        if ($scope.listaFiltro.length > 0) {
                            $scope.tableParams = new NgTableParams({
                                page: 1,
                                count: 9,
                            }, {
                                    counts: [],
                                    total: $scope.listaFiltro.length,
                                    getData: function (params) {
                                        var orderedData = params.sorting() ? $filter('orderBy')($scope.listaFiltro, params.orderBy()) : $scope.proyectoAsignado;
                                        return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                    }
                                });
                        }

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();
        $scope.obtenerServicioRealizado();



        $scope.modificar = function (detalle) {
            $localStorage.modificarDatos = detalle;
            $state.go('app.modificar-servicio-realizado-detalles');
        };
        
        $scope.openModal = function (data) {
            if($scope.selected.datos.pagado){                
                toastr.warning('El servicio ya se encuentra pagado y cerrado.');
                return;
            }
            $scope.detalle = data;
            $('#eliminar').modal('toggle');
        };

        $scope.eliminar = function () {
            var param = {
                "idServicio": $scope.detalle.idServicio,
                "idServicioRealizado": $scope.detalle.idServicioRealizado,
                "idServicioRealizadoDetalles": $scope.detalle.idServicioRealizadoDetalles,
                /* "idEmpleado": $scope.selected.servicioRealizado.idEmpleado */
                "idEmpleado": $scope.detalle.idEmpleado
            }

            MainFactory.eliminarServicioRealizadoDetales(param, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        toastr.success('Se eliminó el servicio realizado detalle.',"", {
                            "timeOut": "1650"
                        });
                        $scope.listar();
                        $scope.obtenerServicioRealizado(); 
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    $('#eliminar').modal('hide'); 
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    $('#eliminar').modal('hide');  
                    unBlockUI();
                });


        };

        $scope.agregar = function (servicioRealizado) {
            if($scope.selected.datos.pagado){                
                toastr.warning('El servicio ya se encuentra pagado y cerrado.');
                return;
            }
            $localStorage.servicioRealizado = servicioRealizado;
            $state.go('app.agregar-servicio-realizado-detalles',{},{reload:true});
        };


        $scope.pagar = function (servicioRealizado) {
            alert("Pagar");
        };
        
        $scope.pagar = function () {
            $localStorage.datos = $scope.selected.datos;
            $state.go('app.cobranza-detalles');
        };
    }]);