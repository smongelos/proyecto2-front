app.controller('ModificarServicioRealizadoDetallesCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        console.log($scope.selected.modificarDatos)
        tipo = 'empleado-detalles';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            console.log('hola')
            $('#cambioEmpleado').modal('toggle');
        }

        $scope.rubros;
        parametros = 'rubro';
        blockUI();
        $scope.rubros = [];
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.rubros = response.data.lista;
                   
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                unBlockUI();
            });

        $scope.modificarPersona = function () {

            var parametros = {

                "idEmpleadoDetalles": $scope.selected.modificarDatos.idEmpleadoDetalles,
                "fechaAsignacion": $scope.selected.modificarDatos.fecha_asignacion,
                "idEmpleado": $scope.selected.modificarDatos.idEmpleado,
                "idRubro": $scope.selected.modificarDatos.idRubro,
                "salario": $scope.selected.modificarDatos.salario
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        $state.go('app.empleado-detalle')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.usuario');
        };
    }]);