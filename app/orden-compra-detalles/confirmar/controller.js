app.controller('ConfirmarRecepcionOrdenCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.selected = {};
        $scope.selected.datos = $localStorage.datos;
        // console.log($scope.selected.datos)
        $scope.idOrdenCompra = $scope.selected.datos.idOrdenesCompra;
        // console.log($scope.idOrdenCompra)
        $scope.detalle;
        $scope.listaFiltro = [];
        $scope.datos = {};
        $scope.datos.costoTotal;
        tipo = "ordenes-compra-detalles";

        $scope.obtenerOrdenesCompra = function () {
            blockUI();
            MainFactory.listaOrdenCompra()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.detalle = response.data.lista;
                        // console.log($scope.detalle)
                        $scope.auxi = $scope.selected.datos;
                        // console.log($scope.auxi.idEstado)
                        for (var i in $scope.detalle) {
                            if ($scope.detalle[i].idOrdenesCompra == $scope.auxi.idOrdenesCompra) {
                                $scope.selected.datos = $scope.detalle[i];
                                // console.log($scope.selected.datos)
                            }
                        }
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar = function () {
            blockUI();
            MainFactory.listaOrdenCompraDetalles()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.detalle = response.data.lista;
                        // console.log($scope.detalle)
                        $scope.auxi = $scope.selected.datos;
                        // console.log($scope.auxi)
                        for (var i in $scope.detalle) {
                            if ($scope.detalle[i].idOrdenesCompra == $scope.auxi.idOrdenesCompra) {
                                $scope.listaFiltro.push($scope.detalle[i]);
                            }
                        }
                        if ($scope.listaFiltro.length > 0) {
                            console.log($scope.listaFiltro);
                            $scope.tableParams = new NgTableParams({
                                sorting: {
                                    descripcion: 'asc'
                                },
                                page: 1,
                                count: 5,
                            }, {
                                    counts: [],
                                    total: $scope.listaFiltro.length,
                                    getData: function (params) {
                                        var orderedData = params.sorting() ? $filter('orderBy')($scope.listaFiltro, params.orderBy()) : $scope.proyectoAsignado;
                                        return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                    }
                                });
                        }

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();
        $scope.obtenerOrdenesCompra();

        $scope.modificar = function (detalle) {
            $localStorage.modificarDatos = detalle;
            console.log(detalle)
            $state.go('app.modificar-confimacion-orden');
        };

        $scope.openModal = function (detalle) {
            $localStorage.eliminarDatos = detalle;
            // console.log($localStorage.eliminarDatos.idOrdenesCompraDetalles)
            nroId = $localStorage.eliminarDatos.idOrdenesCompraDetalles;
            $('#eliminarOrdenCompraDetalle').modal('toggle');
        };

        $scope.eliminar = function (detalle) {
            parametros = 'ordenes-compra-detalles';
            // console.log($localStorage.eliminarDatos.idOrdenesCompraDetalles)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    $('#eliminarOrdenCompraDetalle').modal('hide');
                    if (response.status === 204) {
                        toastr.success('Se eliminó el producto');
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        };

        $scope.agregar = function (ordenCompraDetalle) {
            $localStorage.ordenCompraDetalle = ordenCompraDetalle;
            // console.log(ordenCompraDetalle)
            $state.go('app.agregar-orden-compra-detalles',{},{reload:true});
        };

        $scope.generarOrden = function () {
            tipo = 'ordenes-compra';
            var parametros = {
                "idOrdenesCompra": $scope.selected.datos.idOrdenesCompra,
                "fecha": $scope.selected.datos.fecha,
                "idEstado": $scope.selected.datos.idEstado,
                "recepcionado":$scope.selected.datos.recepcionado,
                "costoEstimado": $scope.selected.datos.costoEstimado,
                "costoTotal": $scope.selected.datos.costoTotal,
                "idProveedor": $scope.selected.datos.idProveedor,
                "generado": true
            }
            // console.log(parametros)

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    $('#cancelarOrdenCompra').modal('hide');
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success("Orden de compra generada con éxito.","", {
                            "timeOut": "1650"
                        });
                        $state.go("app.orden-compra-detalles",{},{reload:true});
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });
         };

         $scope.confirmarRecepcion = function () {
            param = {"estado":true};
            $scope.caja = [];
            MainFactory.listarCajaFiltrada(param)
            .then(function (response) {
                if (response.status === 200) {
                    console.log(response.data.lista)
                    if(!$scope.verificarCajas(response.data.lista)){
                        toastr.warning('No se encuentra ninguna caja abierta.', 'Atención');}
                    else if ($scope.verificarCajas(response.data.lista)){
                    
                        tipo = 'ordenes-compra';
                        var parametros = {
                            "idOrdenesCompra": $scope.selected.datos.idOrdenesCompra,
                            "fecha": new Date(+new Date($scope.selected.datos.fecha) + 4 * 60 * 60 * 1000),
                            "idEstado": 4,
                            "recepcionado":true,
                            "costoEstimado": $scope.selected.datos.costoEstimado,
                            "costoTotal": formatoMonto($scope.datos.costoTotal),
                            "idProveedor": $scope.selected.datos.idProveedor,
                            "generado": $scope.selected.datos.generado
                        }
                        console.log(parametros)

                        blockUI();
                        MainFactory.modificarDatosActuales(parametros, tipo)
                            .then(function (response) {
                                $('#cancelarOrdenCompra').modal('hide');
                                if (response.status === 200) {
                                    $scope.modificar = response.data.dato;
                                    toastr.success("Orden de compra confirmada con éxito.","", {
                                        "timeOut": "1650"
                                    });
                                    $state.go("app.orden-compra",{},{reload:true});
                                }
                                unBlockUI();
                            },
                                function (response) {
                                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                                    "timeOut": "1650"
                                });
                                    unBlockUI();
                                });


                        $localStorage.datos = ordenCompra;
                        $state.go('app.confirmar-orden-compra');                    
                }}})};
                    
        $scope.verificarCajas = function (listaCajas) {
            for(caja in listaCajas){
                if(listaCajas[caja].estado){
                    return true;
                }
            }
            return false;
        };
        $(document).ready(function(e){
            $('#costoTotal').focus()
        });

    }]);