app.controller('ModificarConfirmacionOrdenCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        $scope.selected.modificarDatos.costo =  formatear($scope.selected.modificarDatos.costo);
        $scope.selected.modificarDatos.cantidad =  formatear($scope.selected.modificarDatos.cantidad);
        // console.log($scope.selected.modificarDatos)
        tipo = 'ordenes-compra-detalles';

        $scope.modificarOrdenesCompraDetalles = function () {

            var parametros = {
                "costo": 0,
                "cantidad": $scope.selected.modificarDatos.cantidad,
                "idProducto": $scope.selected.modificarDatos.idProducto,
                "idOrdenesCompra": $scope.selected.modificarDatos.idOrdenesCompra,
                "idOrdenesCompraDetalles": $scope.selected.modificarDatos.idOrdenesCompraDetalles,
                "fechaVencimiento": $scope.selected.modificarDatos.fechaVencimiento
            }
            console.log(parametros)

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Detalle modificado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.confirmar-orden-compra')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });
        }

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $scope.modificarOrdenesCompraDetalles();
        }

        $scope.cancelar = function () {
            $state.go('app.confirmar-orden-compra');
        };
    }]);