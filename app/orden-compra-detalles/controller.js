app.controller('OrdenCompraDetallesCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        
        blockUI();
        $scope.selected = {};
        $scope.selected.datos = $localStorage.datos;
        console.log($scope.selected.datos)
        $scope.idOrdenCompra = $scope.selected.datos.idOrdenesCompra;
        $scope.detalle;
        $scope.listaFiltro = [];
        tipo = "ordenes-compra-detalles";

        $scope.obtenerOrdenesCompra = function () {
            blockUI();
            MainFactory.listaOrdenCompra()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.detalle = response.data.lista;
                        console.log($scope.detalle)
                        $scope.auxi = $scope.selected.datos;
                        for (var i in $scope.detalle) {
                            if ($scope.detalle[i].idOrdenesCompra == $scope.auxi.idOrdenesCompra) {
                                $scope.selected.datos = $scope.detalle[i];
                                console.log($scope.selected.datos)
                            }
                        }
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar = function () {
            blockUI();
            MainFactory.listaOrdenCompraDetalles()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.detalle = response.data.lista;
                        $scope.auxi = $scope.selected.datos;
                        for (var i in $scope.detalle) {
                            if ($scope.detalle[i].idOrdenesCompra == $scope.auxi.idOrdenesCompra) {
                                $scope.listaFiltro.push($scope.detalle[i]);
                            }
                        }
                        if ($scope.listaFiltro.length > 0) {
                            $scope.tableParams = new NgTableParams({
                                sorting: {
                                    descripcion: 'asc'
                                },
                                page: 1,
                                count: 100,
                            }, {
                                    counts: [],
                                    total: $scope.listaFiltro.length,
                                    getData: function (params) {
                                        var orderedData = params.sorting() ? $filter('orderBy')($scope.listaFiltro, params.orderBy()) : $scope.proyectoAsignado;
                                        return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                    }
                                });
                        }

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }

        $scope.listar();
        $scope.obtenerOrdenesCompra();

        $scope.modificar = function (detalle) {
            $localStorage.modificarDatos = detalle;
            $state.go('app.modificar-servicio-producto');
        };

        $scope.openModal = function (detalle) {
            $localStorage.eliminarDatos = detalle;
            nroId = $localStorage.eliminarDatos.idOrdenesCompraDetalles;
            $('#eliminarOrdenCompraDetalle').modal('toggle');
        };

        $scope.eliminar = function (detalle) {
            parametros = 'ordenes-compra-detalles';

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    $('#eliminarOrdenCompraDetalle').modal('hide');
                    if (response.status === 204) {
                        toastr.success('Se eliminó el producto');
                        $scope.listar();

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
                $state.reload();
        };

        $scope.agregar = function (ordenCompraDetalle) {
            $localStorage.ordenCompraDetalle = ordenCompraDetalle;
            $state.go('app.agregar-orden-compra-detalles',{},{reload:true});
        };

        $scope.print = function (divName) {

            var printContents = $('#printableArea').clone().find('script').remove().end().html();

            var allLinks = $('head').clone().find('script').remove().end().html();
            
            var popupWin = window.open('', '_blank');
            popupWin.document.open();

            var keepColors = '<style>body {-webkit-print-color-adjust: exact !important; }</style>';

            popupWin.document.write('<html><head>' + keepColors + allLinks + '</head><body>' + printContents + '</body></html>');
            popupWin.document.close();           

            $(popupWin).on('load', function () {
                setTimeout(function () { popupWin.print(); }, '2,9');
                setTimeout(function () { popupWin.close(); }, 5);
            })

            // popupWin.onload(function(e){
            //      setTimeout(function () { popupWin.print(); }, '2,9');
            //     setTimeout(function () { popupWin.close(); }, 5);
            // });
            // setTimeout(function () { popupWin.print(); }, 500);




            // var printContents = document.getElementById(divName).innerHTML;
            // var originalContents = document.body.innerHTML;       
            // document.body.innerHTML = printContents;       
            // window.print();       
            // document.body.innerHTML = originalContents;            
            // window.location.reload();
       };

        $scope.generarOrden = function () {

            param = {"estado":true};
            $scope.caja = [];
            MainFactory.listarCajaFiltrada(param)
                .then(function (response) {
                    if (response.status === 200) {
                        console.log(response.data.lista)
                        if(!$scope.verificarCajas(response.data.lista)){
                            toastr.warning('No se encuentra ninguna caja abierta.', 'Atención');}
                            else if ($scope.verificarCajas(response.data.lista) && $scope.listaFiltro.length > 0){

                                tipo = 'ordenes-compra';
                                var parametros = {
                                    "idOrdenesCompra": $scope.selected.datos.idOrdenesCompra,
                                    "fecha": new Date(+new Date($scope.selected.datos.fecha) + 4 * 60 * 60 * 1000),
                                    "idEstado": $scope.selected.datos.idEstado,
                                    "recepcionado":$scope.selected.datos.recepcionado,
                                    "costoEstimado": $scope.selected.datos.costoEstimado,
                                    "costoTotal": $scope.selected.datos.costoTotal,
                                    "idProveedor": $scope.selected.datos.idProveedor,
                                    "generado": true
                                }
                                blockUI();
                                MainFactory.modificarDatosActuales(parametros, tipo)
                                    .then(function (response) {
                                        $('#cancelarOrdenCompra').modal('hide');
                                        if (response.status === 200) {
                                            $scope.modificar = response.data.dato;
                                            toastr.success("Orden de compra generada con éxito.","", {
                                                "timeOut": "1650"
                                            });
                                            $state.go("app.orden-compra-detalles",{},{reload:true});
                                        }
                                        unBlockUI();
                                    },
                                        function (response) {
                                            toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                                            "timeOut": "1650"
                                        });
                                            unBlockUI();
                                        });
                             }else {
                                toastr.warning('Debes agregar al menos un producto.', 'ATENCIÓN', {
                                    "timeOut": "2500"
                                });}


                }});           
            };

        //  $scope.modificar = function () {

        //     var parametros = {
        //         "costo": 0,
        //         "cantidad": $scope.selected.modificarDatos.cantidad,
        //         "idProducto": $scope.selected.modificarDatos.idProducto,
        //         "idOrdenesCompra": $scope.selected.modificarDatos.idOrdenesCompra,
        //         "idOrdenesCompraDetalles": $scope.selected.modificarDatos.idOrdenesCompraDetalles,
        //         "fechaVencimiento": $scope.selected.modificarDatos.fechaVencimiento
        //     }
        //     console.log(parametros)

        //     blockUI();
        //     MainFactory.modificarDatosActuales(parametros, tipo)
        //         .then(function (response) {
        //             if (response.status === 200) {
        //                 $scope.modificar = response.data.dato;
        //                 toastr.success('Detalle modificado con éxito.',"", {
        //                     "timeOut": "1650"
        //                 });
        // //                 $state.go('app.confirmar-orden-compra')
        //             }
        //             unBlockUI();
        //         },
        //             function (response) {
        //                 toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
        //                 "timeOut": "1650"
        //             });
        //                 unBlockUI();
        //             });
        // };

         $scope.confirmar = function (ordenCompra) {
            $localStorage.datos = ordenCompra;
            $state.go('app.confirmar-orden-compra');
        };

        $scope.verificarCajas = function (listaCajas) {
            for(caja in listaCajas){
                if(listaCajas[caja].estado){
                    console.log(listaCajas[caja])
                    return true;
                }
            }
            return false;
        };
        $(document).ready(function(e){
            $('#agregar').focus()
        });
        

    }]);