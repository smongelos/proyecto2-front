app.controller('AgregarOrdenCompraDetallesCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.datos = {};
        $scope.selected = {};
        $scope.selected.ordenCompraDetalle = $localStorage.ordenCompraDetalle;
        console.log($scope.selected.ordenCompraDetalle)
        $scope.datos.idServicioProductos;
        $scope.datos.idProducto;
        $scope.datos.cantidad;
        $scope.datos.costo = 0;

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.personas = response.data.lista;                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }
        
        $scope.producto;
        parametros = 'producto';
        blockUI();
        $scope.producto = [];
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.producto = response.data.lista;
                    console.log($scope.producto)
                   
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                unBlockUI();
            });


        $scope.listar();
        $(".opciones").trigger('chosen:updated');
        $scope.insertar = function () {

            tipo = "ordenes-compra-detalles";
            parametros = {
                "idProducto": $scope.datos.idProducto,
                "idOrdenesCompra": $scope.selected.ordenCompraDetalle.idOrdenesCompra,
                "cantidad": $scope.datos.cantidad,
                "costo": $scope.datos.costo
            }
            console.log(parametros)

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        toastr.success('Producto registrado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go("app.orden-compra-detalles",{},{reload:true});
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }        

        $scope.cancelar = function () {
            $state.go('app.orden-compra-detalles');
        };

        $scope.limpiar = function () {
            $scope.datos.idProducto = "";
            $scope.datos.cantidad = "";
        };
        $(document).ready(function(e){
            $('#producto').focus()
        });
 

 
    }]);