app.controller('ProveedorCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.proveedor;
        parametros = 'proveedor';

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.proveedores = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                nombre: 'asc'
                            },
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.proveedores.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.proveedores, params.orderBy()) : $scope.proveedores;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();



        $scope.modificar = function (proveedor) {
            $localStorage.modificarDatos = proveedor;
            $state.go('app.modificar-proveedor');
        };

        $scope.openModal = function (proveedor) {
            $localStorage.eliminarDatos = proveedor;
            nroId = $localStorage.eliminarDatos.idProveedor;
            $('#eliminarProveedor').modal('toggle');
        };

        $scope.eliminar = function (proveedor) {
            console.log($localStorage.eliminarDatos.idProveedor)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success("Se eliminó el proveedor.","", {
                            "timeOut": "1650"
                        });
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
            $('#eliminarProveedor').modal('hide');                
        };
        $scope.agregar = function () {
            $state.go('app.agregar-proveedor');
        };
    }]);