app.controller('AgregarProveedorCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};
        $scope.datos.nombre;
        $scope.datos.ruc;
        $scope.datos.telefono;
        $scope.datos.direccion;

        /*$scope.formatDate = function (date) {
            var dateobj = date;
            var month = dateobj.getMonth() + 1;
            var day = dateobj.getDate();
            var year = dateobj.getFullYear();
            return day + '/' + month + '/' + year;
        }*/


        $scope.insertar = function () {
            tipo = "proveedor";
            parametros = {
                "nombre": $scope.datos.nombre,
                "ruc": $scope.datos.ruc,
                "telefono": $scope.datos.telefono,
                "direccion": $scope.datos.direccion
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        toastr.success('Proveedor registrado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.proveedor');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }

        $scope.cancelar = function () {
            $state.go('app.proveedor');
        };

        $scope.limpiar = function () {
            $scope.datos.nombre = "";
            $scope.datos.ruc = "";
            $scope.datos.telefono = "";
            $scope.datos.direccion = "";
        };
        $(document).ready(function(e){
            $('#nombre').focus()
        });
    }]);