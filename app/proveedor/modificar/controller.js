app.controller('ModificarProveedorCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        console.log($scope.selected.modificarDatos)
        tipo = 'proveedor';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            console.log('hola')
            $('#cambioProveedor').modal('toggle');
        }

        $scope.modificarProveedor = function () {

            var parametros = {
                "nombre": $scope.selected.modificarDatos.nombre,
                "idProveedor": $scope.selected.modificarDatos.idProveedor,
                "ruc": $scope.selected.modificarDatos.ruc,
                "telefono": $scope.selected.modificarDatos.telefono,
                "direccion": $scope.selected.modificarDatos.direccion
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Proveedor modificado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.proveedor')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });
        }

        $scope.cancelar = function () {
            $state.go('app.proveedor');
        };
        $(document).ready(function(e){
            $('#nombre').focus()
        });
    }]);