app.controller('AgregarOrdenCompraCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};
       
        $scope.datos.idProveedor;
        $scope.datos.fecha = new Date();
        $scope.datos.idEstado = 9;
        $scope.datos.recepcionado = false;
        $scope.datos.costoEstimado = 0;
        $scope.datos.costoTotal = null;
        $scope.datos.generado = false;
        $scope.nombre = $localStorage.empleado.nombre;

        $scope.listar = function () {
            blockUI();
            MainFactory.listaCliente()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.cliente = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();
        $(".opciones").trigger('chosen:updated');
        
        $scope.proveedor;
        parametros = 'proveedor';
        blockUI();
        $scope.proveedor = [];
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.proveedor = response.data.lista;
                   
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                unBlockUI();
            });

        $scope.listar();

        $scope.insertar = function () {

            tipo = "ordenes-compra";
            parametros = {
                "fecha" : $scope.datos.fecha,
                "idProveedor" : $scope.datos.idProveedor,
                "costoEstimado": $scope.datos.costoEstimado,
                "idEstado": $scope.datos.idEstado,
                "recepcionado" : $scope.datos.recepcionado,
                "costoTotal": $scope.datos.costoTotal,
                "generado" : $scope.datos.generado
            }

            console.log(parametros)

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;

                        $state.go('app.orden-compra');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.orden-compra');
        };

        $scope.limpiar = function () {
            $scope.datos.idProveedor = "";
        };
        $(document).ready(function(e){
            $('#proveedor').focus()
        });
 

 
    }]);