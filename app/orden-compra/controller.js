app.controller('OrdenCompraCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        blockUI();
        $scope.ordenCompra;
        $scope.pendiente = false;
        // parametros = 'empleado';
        tipo = 'orden-compra';

        $scope.listar = function () {
            blockUI();
            MainFactory.listaOrdenCompra()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.ordenCompra = response.data.lista;
                        console.log($scope.ordenCompra)
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                idOrdenesCompra: 'desc'
                            },
                            page: 1,
                            count: 5,
                        }, {
                                counts: [],
                                total: $scope.ordenCompra.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.ordenCompra, params.orderBy()) : $scope.ordenCompra;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }

        $scope.listar();

        $scope.modificar = function (ordenCompra) {
            $localStorage.modificarDatos = ordenCompra;
            $state.go('app.modificar-orden-compra');
        };

        $scope.agregar = function () {
            $state.go('app.agregar-orden-compra',{},{reload:true});
        };

        $scope.verMas = function (ordenCompra) {
            $localStorage.datos = ordenCompra;
            $state.go('app.orden-compra-detalles');
        };
        
        $scope.confirmar = function (ordenCompra) {
            $localStorage.datos = ordenCompra;
            $state.go('app.confirmar-orden-compra');
        };

        $scope.openModal = function (detalle) {
            $localStorage.datosCancelar = detalle;
            console.log($localStorage.datosCancelar.idOrdenesCompra)
            // nroId = $localStorage.datosCancelar.idOrdenesCompra;
            $('#cancelarOrdenCompra').modal('toggle');
        };
        

        $scope.cancelarOrden = function () {

            param = {"fecha" : new Date().getTime(),
                    "estado":true};
            $scope.caja = [];
            MainFactory.listarCajaFiltrada(param)
                .then(function (response) {
                   if (response.status === 200) {
                       if(!$scope.verificarCajas(response.data.lista)){
                           toastr.warning('No se encuentra ninguna caja abierta.', 'Atención');
                           $('#cancelarOrdenCompra').modal('hide');}
                        else if ($scope.verificarCajas(response.data.lista)){

                            tipo = 'ordenes-compra';
                            var parametros = {
                                "idOrdenesCompra": $localStorage.datosCancelar.idOrdenesCompra,
                                "fecha": new Date(+new Date($localStorage.datosCancelar.fecha)+ 4 * 60 * 60 * 1000),
                                "idEstado": 18,
                                "recepcionado":$localStorage.datosCancelar.recepcionado,
                                "costoEstimado": $localStorage.datosCancelar.costoEstimado,
                                "costoTotal": 0,
                                "idProveedor": $localStorage.datosCancelar.idProveedor,
                                "generado":$localStorage.datosCancelar.generado,
                            }
                            console.log(parametros)

                            blockUI();
                            MainFactory.modificarDatosActuales(parametros, tipo)
                                .then(function (response) {
                                    $('#cancelarOrdenCompra').modal('hide');
                                    if (response.status === 200) {
                                        $scope.modificar = response.data.dato;
                                        toastr.success("Orden de compra cancelada con éxito.","", {
                                            "timeOut": "1650"
                                        });
                                        $state.go("app.orden-compra",{},{reload:true});
                                    }
                                    unBlockUI();
                                },
                                    function (response) {
                                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                                        "timeOut": "1650"
                                    });
                                        unBlockUI();
                                    });}
                                }});
                            };

         $scope.ordenPendiente  = function () {
            MainFactory.listaOrdenCompra()            
            .then(function (response) {
                if (response.status === 200) {
                    $scope.detalle = response.data.lista;
                    console.log($scope.detalle)
                    for (var i in $scope.detalle) {
                        if ($scope.detalle[i].generado & $scope.detalle[i].idEstado == 9 ) {
                            $scope.pendiente = true;
                        }
                    }            
                }
            console.log($scope.pendiente);
            });
        }
        $scope.ordenPendiente();

        $scope.verificarCajas = function (listaCajas) {
            for(caja in listaCajas){
                if(listaCajas[caja].estado){
                    return true;
                }
            }
            return false;
        };

    }]);