app.controller('ClienteCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        

       blockUI();
        $scope.cliente;
        // var parametro = {};

        $scope.listar = function () {
            blockUI();
            MainFactory.listaCliente()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.cliente = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                fecha_creacion: 'desc'
                            },    
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.cliente.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.cliente, params.orderBy()) : $scope.cliente;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                   unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();



        $scope.openModal = function (cliente) {
            $localStorage.eliminarDatos = cliente;
             nroId = $localStorage.eliminarDatos.id_cliente;
            $('#eliminarCliente').modal('toggle');
        };


        $scope.eliminar = function (cliente) {
            tipo = 'cliente'

            MainFactory.eliminarDatos(tipo, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó el cliente.',"", {
                            "timeOut": "1650"
                        });
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

                $('#eliminarCliente').modal('toggle');


        };

        $scope.modificar = function (cliente) {
            $localStorage.modificarDatos = cliente;
            $state.go('app.modificar-cliente');
        };


        $scope.agregar = function () {
           $state.go('app.agregar-cliente',{},{reload:true});
        };

    }]);