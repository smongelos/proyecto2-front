app.controller('ModificarClienteCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        tipo = 'cliente';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#cambioCliente').modal('toggle');
        }

        $scope.modificarCliente = function () {

            var parametros = {
                "idCliente": $scope.selected.modificarDatos.id_cliente,
                "ruc": $scope.selected.modificarDatos.ruc,
                "fechaCreacion": $scope.selected.modificarDatos.fecha_creacion,
                "idPersona": $scope.selected.modificarDatos.idPersona
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Cliente modificado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.cliente')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.cliente');
        };
        $(document).ready(function(e){
            $('#ruc').focus()
        });
    }]);