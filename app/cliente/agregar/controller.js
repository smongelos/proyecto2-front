app.controller('AgregarClienteCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};
        $scope.datos.origen;       
        $scope.datos.idPersona;
    
        parametros = 'persona'

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.personas = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();
        $(".opciones").trigger('chosen:updated');

        $scope.insertar = function () {

            $scope.datos.fechaCreacion = new Date();
            
            tipo = "cliente";
            parametros = {
                "fechaCreacion": $scope.datos.fechaCreacion,
                "idPersona": $scope.datos.origen.idPersona,
                "ruc": $scope.datos.origen.documento
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        toastr.success('Cliente registrado con éxito.',"", {
                            "timeOut": "1650"
                        });

                        $state.go('app.cliente');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.cliente');
        };

        $scope.limpiar = function () {
            $scope.datos.origen.documento = "";
            $scope.datos.origen = "";
        };

        $(document).ready(function(e){
            $('#nombre').focus()
        });
 

 
    }]);