app.controller('ModificarServicioRealizadoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.cliente = [];
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        $scope.selected.modificarDatos.fechaOperacion = new Date(+new Date($scope.selected.modificarDatos.fechaOperacion) + 4 * 60 * 60 * 1000);
        // $scope.selected.modificarDatos.fechaOperacion = new Date( $scope.selected.modificarDatos.fechaOperacion );
        console.log($scope.selected.modificarDatos)
        tipo = 'servicio-realizado';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#cambioEmpleado').modal('toggle');
        }

        $scope.modificarServicioRealizado = function () {

            var parametros = {
                "fechaOperacion" : $scope.selected.modificarDatos.fechaOperacion,
                "idCliente" : $scope.selected.modificarDatos.idCliente,
                "montoTotal": $scope.selected.modificarDatos.montoTotal,
                "idServicioRealizado": $scope.selected.modificarDatos.idServicioRealizado
            }
            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        $state.go('app.servicio-realizado')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });



        }
        
        $scope.listar = function () {
            blockUI();
            MainFactory.listaCliente()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.cliente = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
            }

        $scope.cancelar = function () {
            $state.go('app.servicio-realizado');
        };

        
        $scope.listar();
        $(document).ready(function(e){
            $('#cliente').focus()
        });
    }]);