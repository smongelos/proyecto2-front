app.controller('AgregarServicioRealizadoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};
       
        $scope.datos.idCliente = "";
        $scope.datos.fechaOperacion = new Date();
        
        $scope.listar = function () {
            blockUI();
            MainFactory.listaCliente()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.cliente = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();
        $(".opciones").trigger('chosen:updated');
        
        $scope.insertar = function () {

            tipo = "servicio-realizado";
            parametros = {
                "fechaOperacion" : $scope.datos.fechaOperacion,
                "idCliente" : $scope.datos.idCliente,
                "montoTotal": 0,
                "pagado": false
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;

                        $state.go('app.servicio-realizado');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.servicio-realizado');
        };

        $scope.limpiar = function () {
            $scope.datos.fechaOperacion = "";
            $scope.datos.idCliente = "";
            // $scope.datos.pass = "";
            // $scope.datos.telefono = "";
        };
        $(document).ready(function(e){
            $('#cliente').focus()
        });
 

 
    }]);