app.controller('ServicioRealizadoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.servicioRealizado;
        parametros = 'empleado';
        tipo = 'servicio-realizado';

        $scope.listar = function () {
            blockUI();
            MainFactory.listaServicioRealizado()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.servicioRealizado = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                idServicioRealizado: 'desc'
                            },
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.servicioRealizado.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.servicioRealizado, params.orderBy()) : $scope.servicioRealizado;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();



        $scope.modificar = function (servicioRealizado) {
            $localStorage.modificarDatos = servicioRealizado;
            $state.go('app.modificar-servicio-realizado');
        };

        $scope.openModal = function (data) {
            $scope.idDelete = data;
            $('#eliminar').modal('toggle');
        };
        $scope.eliminar = function () {
             MainFactory.eliminarDatos(tipo, $scope.idDelete)
                 .then(function (response) {
                     if (response.status === 204) {
                         toastr.success('Se eliminó el servicio realizado.');
                         $scope.listar();
                     } else {
                         toastr.info(response.data.mensaje);
                     }
                     $('#eliminar').modal('hide');  
                     unBlockUI();
                 }, function (response) {
                     toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                     $('#eliminar').modal('hide');  
                     unBlockUI();
                 });
 
 
         };

        $scope.agregar = function () {
            $state.go('app.agregar-servicio-realizado',{},{reload:true});
        };

        $scope.verMas = function (servicioRealizado) {
            $localStorage.datos = servicioRealizado;
            $state.go('app.servicio-realizado-detalles');
        };
        
        $scope.pagar = function (servicioRealizado) {
            $localStorage.datos = servicioRealizado;
            $state.go('app.cobranza-detalles');
        };

    }]);