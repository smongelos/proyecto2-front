app.controller('ServicioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.servicio;
        parametros = 'servicio';

        $scope.listar = function () {
            blockUI();
            MainFactory.listaServicio()
                .then(function (response) {
                    if (response.status === 200) {

                        $scope.servicio = response.data.lista;

                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                descripcion: 'asc'
                            },
                            page: 1,
                            count: 7,
                        }, {
                                counts: [],
                                total: $scope.servicio.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.servicio, params.orderBy()) : $scope.servicio;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();



        $scope.modificar = function (servicio) {
            $localStorage.modificarDatos = servicio;
            $state.go('app.modificar-servicio');
        };

        $scope.openModal = function (servicio) {
            $localStorage.eliminarDatos = servicio;
            nroId = $localStorage.eliminarDatos.id_servicio;
            $('#eliminarServicio').modal('toggle');
        };

        $scope.eliminar = function (servicio) {
            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó el servicio.');
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
            $('#eliminarServicio').modal('hide');
            };

        $scope.agregar = function () {
            $state.go('app.agregar-servicio',{},{reload:true});
        };

        $scope.verMas = function (servicio) {
            $localStorage.datos = servicio;
            $state.go('app.servicio-producto');
        };
    }]);