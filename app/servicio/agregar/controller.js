app.controller('AgregarServicioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};
       
        $scope.datos.idRubro;
        $scope.datos.descripcion;
        $scope.datos.precio;
        $scope.datos.canProducto;
        $scope.datos.costoProducto;
        $scope.datos.iva;
        
        parametros = 'rubro'

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.rubros = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();
        $(".opciones").trigger('chosen:updated');
        
        $scope.insertar = function () {

            tipo = "servicio";
            parametros = {
                "descripcion": $scope.datos.descripcion,
                "precio" : formatoMonto($scope.datos.precio),
                "cantProducto" : 0,
                "costoProducto" : 0,
                "iva" : $scope.datos.iva,
                "idRubro" : $scope.datos.idRubro
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        toastr.success('Servicio registrado con éxito.',"", {
                            "timeOut": "1650"
                        });

                        $state.go('app.servicio');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.servicio');
        };

        $scope.limpiar = function () {
            $scope.datos.descripcion = "";
            $scope.datos.precio = "";
            $scope.datos.iva = "";
            $scope.datos.idRubro = "";
        };
        $(document).ready(function(e){
            $('#rubro').focus()
        });

 

 
    }]);