app.controller('ModificarServicioCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {
        
        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        $scope.selected.modificarDatos.precio =  formatear($scope.selected.modificarDatos.precio);
        tipo = 'servicio';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#cambioServicio').modal('toggle');
        }

        $scope.rubros;
        parametros = 'rubro';
        blockUI();
        $scope.rubros = [];
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.rubros = response.data.lista;
                   
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                unBlockUI();
            });


        $scope.modificarServicio = function () {

            var parametros = {

                "idServicio": $scope.selected.modificarDatos.id_servicio,
                "descripcion": $scope.selected.modificarDatos.descripcion,
                "precio": formatoMonto($scope.selected.modificarDatos.precio),
                "cantProducto": 0,
                "costoProducto": 0,
                // "cantProducto": $scope.selected.modificarDatos.cant_producto,
                // "costoProducto": $scope.selected.modificarDatos.costo_producto,
                "iva": $scope.selected.modificarDatos.iva,
                "idRubro": $scope.selected.modificarDatos.id_rubro
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Servicio modificado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.servicio')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });
        }

        $scope.cancelar = function () {
            $state.go('app.servicio');
        };
        $(document).ready(function(e){
            $('#idRubro').focus()
        });
    }]);