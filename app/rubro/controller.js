app.controller('RubroCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.rubro;
        parametros = 'rubro';

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.rubro = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                descripcion: 'asc'
                            },
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.rubro.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.rubro, params.orderBy()) : $scope.rubro;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();

        $scope.modificar = function (rubro) {
            $localStorage.modificarDatos = rubro;
            $state.go('app.modificar-rubro');
        };
      
        $scope.openModal = function (rubro) {
            $localStorage.eliminarDatos = rubro;
            nroId = $localStorage.eliminarDatos.idRubro;
            $('#eliminarRubro').modal('toggle');
        };

        $scope.eliminar = function (rubro) {
            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó el rubro.');
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
            $('#eliminarRubro').modal('hide');
        };

        $scope.agregar = function () {
            $state.go('app.agregar-rubro');
        };
    }]);