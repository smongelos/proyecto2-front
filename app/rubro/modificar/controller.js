app.controller('ModificarRubroCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        tipo = 'rubro';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#cambioRubro').modal('toggle');
        }

        $scope.modificarRubro = function () {

            var parametros = {
                "descripcion": $scope.selected.modificarDatos.descripcion,
                "idRubro": $scope.selected.modificarDatos.idRubro,
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Rubro modificado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.rubro')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });
        }

        $scope.cancelar = function () {
            $state.go('app.rubro');
        };
        $(document).ready(function(e){
            $('#descripcion').focus()
        });
    }]);