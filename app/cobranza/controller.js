app.controller('CobranzaCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.servicioRealizado;

        $scope.listar = function () {
            blockUI();
            MainFactory.listaServicioRealizado(null, 'false')
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.servicioRealizado = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.servicioRealizado.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.servicioRealizado, params.orderBy()) : $scope.servicioRealizado;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();

        $scope.pagar = function (servicioRealizado) {
            $localStorage.datos = servicioRealizado;
            $state.go('app.cobranza-detalles');
        };

    }]);