app.controller('InventarioDetallesCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.selected = {};
        $scope.selected.datos = $localStorage.datos;
        $scope.detalle;
        $scope.listaFiltro = [];
        $scope.realizar = false;
        parametros = 'inventario';

        var modal = document.getElementById("myModal");

        $scope.listar = function () {
            blockUI();
            MainFactory.listaInventarioDetalles()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.detalle = response.data.lista;
                        $scope.auxi = $scope.selected.datos;
                        // console.log($scope.selected.datos);
                        for (var i in $scope.detalle) {
                            if ($scope.detalle[i].idInventario == $scope.auxi.idInventario) {
                                $scope.listaFiltro.push($scope.detalle[i]);
                            }
                        }
                        if ($scope.listaFiltro.length > 0) {
                            // console.log($scope.listaFiltro);
                            $scope.tableParams = new NgTableParams({
                                sorting: {
                                    realizarCompra: 'desc',
                                    producto: 'asc'
                                },
                                page: 1,
                                count: 5,
                            }, {
                                    counts: [],
                                    total: $scope.listaFiltro.length,
                                    getData: function (params) {
                                        var orderedData = params.sorting() ? $filter('orderBy')($scope.listaFiltro, params.orderBy()) : $scope.proyectoAsignado;
                                        return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                    }
                                });
                        }

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();

        $scope.realizarCompra  = function () {
            MainFactory.listaInventarioDetalles()            
            .then(function (response) {
                if (response.status === 200) {
                    $scope.detalle = response.data.lista;
                    $scope.auxi = $scope.selected.datos;
                    for (var i in $scope.detalle) {
                        if ($scope.detalle[i].idInventario == $scope.auxi.idInventario) {
                            if ($scope.detalle[i].realizarCompra == true ){
                                $scope.realizar = true;
                            }
                        }
                    }            
                }
            console.log($scope.realizar);
            });
        }
        $scope.realizarCompra();
        
    }])

