app.controller('AgregarPersonaCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};
        $scope.datos.nombre;
        $scope.datos.apellido;
        $scope.datos.email;
        $scope.datos.documento;
        $scope.datos.fechaNacimiento = new Date();
        $scope.datos.telefono;
        $scope.datos.domicilio;

        /*$scope.formatDate = function (date) {
            var dateobj = date;
            var month = dateobj.getMonth() + 1;
            var day = dateobj.getDate();
            var year = dateobj.getFullYear();
            return day + '/' + month + '/' + year;
        }*/


        $scope.insertar = function () {
            tipo = "persona";
            parametros = {
                "nombres": $scope.datos.nombre,
                "apellidos": $scope.datos.apellido,
                "email": $scope.datos.email,
                "documento": formatoMonto($scope.datos.documento),
                "fechaNacimiento": $scope.datos.fechaNacimiento,
                "telefono": $scope.datos.telefono,
                "domicilio": $scope.datos.domicilio
            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        toastr.success('Persona registrada con éxito.',"", {
                            "timeOut": "1650"
                        });

                        $state.go('app.persona');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }



        $scope.cancelar = function () {
            $state.go('app.persona');
        };

        $scope.limpiar = function () {
            $scope.datos.nombre = "";
            $scope.datos.apellido = "";
            $scope.datos.email = "";
            $scope.datos.documento = "";
            $scope.datos.fechaNacimiento = "";
            $scope.datos.telefono = "";
            $scope.datos.domicilio = "";
        };

        $(document).ready(function(e){
            $('#nombre').focus()
        });

    }]);