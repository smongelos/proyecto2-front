app.controller('ModificarPersonaCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        $scope.selected.modificarDatos.documento = formatear($scope.selected.modificarDatos.documento);
        $scope.selected.modificarDatos.fechaNacimiento = new Date(+new Date($scope.selected.modificarDatos.fechaNacimiento));
        tipo = 'persona';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#cambioPersona').modal('toggle');
        }

        $scope.modificarPersona = function () {

            var parametros = {
                "nombres": $scope.selected.modificarDatos.nombres,
                "idPersona": $scope.selected.modificarDatos.idPersona,
                "apellidos": $scope.selected.modificarDatos.apellidos,
                "email": $scope.selected.modificarDatos.email,
                "documento": formatoMonto($scope.selected.modificarDatos.documento),
                "fechaNacimiento": $scope.selected.modificarDatos.fechaNacimiento,
                "telefono": $scope.selected.modificarDatos.telefono,
                "domicilio": $scope.selected.modificarDatos.domicilio
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Persona modificada con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.persona')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.persona');
        };
    }]);