app.controller('PersonaCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.personas;
        parametros = 'persona';

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.personas = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                nombres: 'asc'
                            },
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.personas.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.personas, params.orderBy()) : $scope.personas;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();



        $scope.modificar = function (persona) {
            $localStorage.modificarDatos = persona;
            $state.go('app.modificar-persona');
        };

        $scope.openModal = function (persona) {
            $localStorage.eliminarDatos = persona;
            nroId = $localStorage.eliminarDatos.idPersona;
            $('#eliminarPersona').modal('toggle');
        };

        $scope.eliminar = function (persona) {

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó la persona.');
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

                $('#eliminarPersona').modal('toggle');


        };

        $scope.agregar = function () {
            $state.go('app.agregar-persona');
        };

    }]);