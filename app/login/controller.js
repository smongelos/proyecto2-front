app.controller('LoginCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.datos = {};

        $scope.login = function () {
            param = {
                "usuario": $scope.datos.user,
                "contrasenha": $scope.datos.pass
            };
            blockUI();
            MainFactory.verificarLogin(param)
                .then(function (response) {
                    if (response.status === 200) {
                        if (response.data.lista.length > 0 && response.data.lista[0] != null) {
                            $state.go('app.inicio');
                            $localStorage.empleado = response.data.lista[0];
                            $localStorage.logueado = true;
                        } else {
                            toastr.warning("El usuario no existe o las credenciales no son correctas.")
                        }

                        unBlockUI();
                    } else {
                        unBlockUI();
                        toastr.info(response.data.mensaje);
                    }
                }, function (response) {
                    toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                    unBlockUI();
                });
        };
        $(document).ready(function(e){
            $('#usuario').focus()
        });

    }]);