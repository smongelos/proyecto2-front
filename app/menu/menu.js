app.factory('MenuFactory', function ($http) {
});

app.controller('MenuCtrl', function ($scope, $state, $localStorage, $rootScope, MainFactory) {

    $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
        if (toState.module === 'private' && !$localStorage.logueado) {
            e.preventDefault();
            $state.go('login');
        }
    });
    $scope.nombre = $localStorage.empleado.nombre;

    $scope.detalle;
    $scope.esAdmin = false;
    $scope.VerificarAdmin  = function () {
        MainFactory.listaEmpleadoDetalles()
        .then(function (response) {
            if (response.status === 200) {
                $scope.detalle = response.data.lista;
                console.log($scope.detalle)
                for (var i in $scope.detalle) {
                    if ($scope.detalle[i].idEmpleado == $localStorage.empleado.idEmpleado & $scope.detalle[i].idRubro == 6 ) {
                        $scope.esAdmin = true;
                    }
                }            
            }
        console.log($scope.esAdmin);
        });
    }
    $scope.VerificarAdmin();

    $scope.salir = function () {
       // $localStorage.logueado = false;
        //$state.go('login');
    };
});