app.controller('ModificarServicioProductoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        console.log($scope.selected.modificarDatos)
        $scope.selected.modificarDatos.costoProducto =  formatear($scope.selected.modificarDatos.costoProducto);
        tipo = 'servicio-productos';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            console.log('hola')
            $('#cambioEmpleado').modal('toggle');
        }

        $scope.productos;
        parametros = 'producto';
        blockUI();
        $scope.productos = [];
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.productos = response.data.lista;
                   
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                unBlockUI();
            });

        $scope.modificarPersona = function () {

            var parametros = {

                "idServicioProductos": $scope.selected.modificarDatos.idServicioProductos,
                "idServicio": $scope.selected.modificarDatos.idServicio,
                "idProducto": $scope.selected.modificarDatos.idProducto,
                "cantProducto": $scope.selected.modificarDatos.cantProducto,
                "costoProducto": formatoMonto($scope.selected.modificarDatos.costoProducto)
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('Detalle Servicio modificado con éxito.',"", {
                            "timeOut": "1650"
                        });
                        $state.go('app.servicio-producto')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.servicio');
        };
        $(document).ready(function(e){
            $('#idProducto').focus()
        });
    }]);