app.controller('AgregarServicioProductoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.detalleServicio = $localStorage.datos;
        console.log($scope.selected.detalleServicio)
        $scope.datos = {};
        $scope.datos.idServicioProductos;
        $scope.datos.idServicio;
        $scope.datos.idProducto;
        $scope.datos.cantProducto;
        $scope.datos.costoProducto;

        $scope.listar = function () {
            blockUI();
            MainFactory.listar(parametros)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.personas = response.data.lista;
                        
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.producto;
        parametros = 'producto';
        blockUI();
        $scope.producto = [];
        MainFactory.listar(parametros)
            .then(function (response) {
                if (response.status === 200) {
                    $scope.producto = response.data.lista;
                    console.log($scope.producto)
                   
                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                unBlockUI();
            });

        $scope.listar();
        $(".opciones").trigger('chosen:updated');

        $scope.insertar = function () {

            tipo = "servicio-productos";
            parametros = {
                "idServicio": $scope.selected.detalleServicio.id_servicio,
                "idProducto": $scope.datos.idProducto,
                "cantProducto": $scope.datos.cantProducto,
                "costoProducto": formatoMonto($scope.datos.costoProducto)

            }

            blockUI();
            MainFactory.agregarDatos(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.agregar = response.data.dato;
                        toastr.success('Producto registrado con éxito.',"", {
                            "timeOut": "1650"
                        });

                        $state.go('app.servicio-producto');
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }


        $scope.cancelar = function () {
            $state.go('app.servicio');
        };

        $scope.limpiar = function () {
            $scope.datos.idProducto = "";
            $scope.datos.cantProducto = "";
            $scope.datos.costoProducto = "";
        };
        $(document).ready(function(e){
            $('#producto').focus()
        });
 

 
    }]);