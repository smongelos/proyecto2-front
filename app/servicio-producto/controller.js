app.controller('ServicioProductoCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.selected = {};
        $scope.selected.datos = $localStorage.datos;
        $scope.detalle;
        $scope.listaFiltro = [];
        parametros = 'servicio';

        $scope.listar = function () {
            blockUI();
            MainFactory.listaServicioProducto()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.detalle = response.data.lista;
                        $scope.auxi = $scope.selected.datos;
                        for (var i in $scope.detalle) {
                            if ($scope.detalle[i].idServicio == $scope.auxi.id_servicio) {
                                $scope.listaFiltro.push($scope.detalle[i]);
                            }
                        }
                        if ($scope.listaFiltro.length > 0) {
                            console.log($scope.listaFiltro);
                            $scope.tableParams = new NgTableParams({
                                sorting: {
                                    descripcion: 'asc'
                                },
                                page: 1,
                                count: 9,
                            }, {
                                    counts: [],
                                    total: $scope.listaFiltro.length,
                                    getData: function (params) {
                                        var orderedData = params.sorting() ? $filter('orderBy')($scope.listaFiltro, params.orderBy()) : $scope.proyectoAsignado;
                                        return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                    }
                                });
                        }

                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();



        $scope.modificar = function (detalle) {
            $localStorage.modificarDatos = detalle;
            $state.go('app.modificar-servicio-producto');
        };

        $scope.openModal = function (detalle) {
            $localStorage.eliminarDatos = detalle;
            nroId = $localStorage.eliminarDatos.idServicioProductos;
            $('#eliminarServicioDetalle').modal('toggle');
        };

        $scope.eliminar = function (detalle) {
            parametros = 'servicio-productos';
            console.log($localStorage.eliminarDatos.idServicioProductos)

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó el producto.',"", {
                            "timeOut": "1650"
                        });
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
            $('#eliminarServicioDetalle').modal('hide');
            $state.reload();

        };

        $scope.agregar = function (detalleServicio) {
            $localStorage.detalleServicio = detalleServicio;
            $state.go('app.agregar-servicio-producto',{},{reload:true});
        };

    }]);