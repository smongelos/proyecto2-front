app.controller('AgregarCajaCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        $scope.datos = {};
        $scope.datos.descripcion;
        $scope.datos.fecha = new Date();

        $scope.nombre = $localStorage.empleado.nombre;
        

      /*  parametros = 'persona'
        blockUI();
        MainFactory.listaEmpleado()
            .then(function (response) {
                if (response.status === 200) {
                    $scope.empleados = response.data.lista;

                } else {
                    toastr.info(response.data.mensaje);
                }
                unBlockUI();
            }, function (response) {
                toastr.info('Ha ocurrido un error, intente nuevamente', 'Atención');
                unBlockUI();
            });
*/

        $scope.insertar = function () {

            blockUI();
            param = { "fecha": new Date().getTime() };
            $scope.caja = [];
            MainFactory.listarCajaFiltrada(param)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.caja = response.data.lista;
                        if (!$scope.verificarCajas($scope.caja)) {
                            tipo = "caja";
                            parametros = {
                                "fecha": new Date().getTime(),
                                "idEmpleado": $localStorage.empleado.idEmpleado,
                                "montoTotalIngreso": 0,
                                "montoTotalEgreso": 0,
                                "estado": true
                            }

                            blockUI();
                            MainFactory.agregarDatos(parametros, tipo)
                                .then(function (response) {
                                    if (response.status === 200) {
                                        $scope.agregar = response.data.dato;
                                        toastr.success('Caja abierta con éxito.', "", {
                                            "timeOut": "1650"
                                        });
                                        $state.go('app.caja');
                                    } else {
                                        toastr.info(response.data.mensaje);
                                    }
                                    unBlockUI();
                                }, function (response) {
                                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                                    unBlockUI();
                                });
                        } else {
                            toastr.warning('La caja ya esta abierta.', 'Atención');
                        }

                    } else {
                        toastr.error(response.data.mensaje);

                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
        }



        $scope.cancelar = function () {
            $state.go('app.caja');
        };

        $scope.limpiar = function () {
            $scope.datos.idEmpleado = "";
        };

        $scope.parseDate = function (data) {
            if (data != null && data !== '') {
                var day = data.getDate();
                var monthIndex = data.getMonth() + 1;
                if (monthIndex < 10) {
                    monthIndex = '0' + monthIndex;
                }
                var year = data.getFullYear();

                return year + '-' + monthIndex + '-' + day;
            }
        };



        $scope.verificarCajas = function (listaCajas) {
            for (caja in listaCajas) {
                if (listaCajas[caja].estado) {
                    return true;
                }
            }
            return false;
        };

    }]);