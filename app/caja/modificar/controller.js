app.controller('ModificarCajaCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {

        $scope.selected = {};
        $scope.selected.modificarDatos = $localStorage.modificarDatos;
        $scope.selected.modificarDatos.fecha = new Date(+new Date($scope.selected.modificarDatos.fecha) + 4 * 60 * 60 * 1000);
        tipo = 'caja';

        $scope.confirmar = function (form) {
            if (!form.$valid) {
                return;
            }
            $('#cambioCaja').modal('toggle');
        }

        $scope.modificar = function () {

            var parametros = {
                "fecha": $scope.selected.modificarDatos.fecha,
                "idEmpleado": $scope.selected.modificarDatos.idEmpleado,
                "montoTotalIngreso": $scope.selected.modificarDatos.monto_total_ingreso,
                "montoTotalEgreso": $scope.selected.modificarDatos.monto_total_egreso,
                "estado": false,
                "idCaja" : $scope.selected.modificarDatos.idCaja
            }

            blockUI();
            MainFactory.modificarDatosActuales(parametros, tipo)
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.modificar = response.data.dato;
                        toastr.success('La caja se cerró.');
                        $state.go('app.caja')
                    }
                    unBlockUI();
                },
                    function (response) {
                        toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                        unBlockUI();
                    });



        }

        $scope.cancelar = function () {
            $state.go('app.caja');
        };
    }]);