app.controller('CajaCtrl', ['$scope', 'MainFactory', '$state', '$sessionStorage', '$localStorage', '$rootScope', '$location', 'NgTableParams', '$filter',
    function ($scope, MainFactory, $state, $sessionStorage, $localStorage, $rootScope, $location, NgTableParams, $filter) {


        blockUI();
        $scope.caj;
        parametros = 'caja';

        $scope.listar = function () {
            blockUI();
            MainFactory.listarCaja()
                .then(function (response) {
                    if (response.status === 200) {
                        $scope.caj = response.data.lista;
                        $scope.tableParams = new NgTableParams({
                            sorting: {
                                fecha: 'desc',
                                estado: 'desc'
                            },
                            page: 1,
                            count: 9,
                        }, {
                                counts: [],
                                total: $scope.caj.length,
                                getData: function (params) {
                                    var orderedData = params.sorting() ? $filter('orderBy')($scope.caj, params.orderBy()) : $scope.caj;
                                    return (orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));
                                }
                            });
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });

        }

        $scope.listar();



        $scope.modificar = function (caja) {
            $localStorage.modificarDatos = caja;
            $state.go('app.modificar-caja');
        };
       
        $scope.openModal = function (producto) {
            $localStorage.eliminarDatos = producto;
            nroId = $localStorage.eliminarDatos.idProducto;
            $('#eliminarProducto').modal('toggle');
        };


        $scope.eliminar = function (producto) {

            MainFactory.eliminarDatos(parametros, nroId)
                .then(function (response) {
                    if (response.status === 204) {
                        toastr.success('Se eliminó el producto.');
                        $scope.listar();
                    } else {
                        toastr.info(response.data.mensaje);
                    }
                    unBlockUI();
                }, function (response) {
                    toastr.warning('Ha ocurrido un error, intente nuevamente.', 'Atención', {
                        "timeOut": "1650"
                    });
                    unBlockUI();
                });
            $('#eliminarProducto').modal('hide');


        };

        $scope.agregar = function () {
            $state.go('app.agregar-caja');
        };

    }]);